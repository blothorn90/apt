﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt
{
    public class MapTile
    {
        private int _column;
        private int _row;
        private int _tileLeft;
        private int _tileBottom;
        private int _tileCenterX;
        private int _tileUnitBottom; // y coordinate of bottom of units, fire, and magic on the tile
        private int _terrainBGTop; // y coordinate of top of terrain background
        private int _unitYOffset = -8; // y offset of units relative to bottom of tile
        private int _paletteIndex = 0; // index of color palette used to indicate tiles available for unit movement
        private readonly static int _paletteIndexLast = 2; // index of last color palette
        private readonly static int _paletteSteps = 18; // game frames between palette frames for animation
        private int _paletteCount = 0; // counter for animating with palette
        private int _terrainFrame = 0; // index of frame to use for animating terrain movement
        private readonly static int _terrainFrameLast = 1; // index of last terrain frame
        private readonly static int _terrainSteps = 36; // game frames between terrain frames for animation
        private int _sourceFrame = 0; // index in source Rectangle array of current tile frame
        private int _terrainStepCount = 0; // counter for animating terrain motion
        private bool _flashing; // whether to animate palette for terrain
        private TerrainType _terrainType;
        private Texture2D _terrainBG;
        private Texture2D _terrainFG;
        private Rectangle[] _terrainBGSourceRectangle = new Rectangle[] {
            new Rectangle(0, 0, GameVariables.TerrainBGWidth, GameVariables.TerrainBGHeight),
            new Rectangle(0, GameVariables.TerrainBGHeight, GameVariables.TerrainBGWidth, GameVariables.TerrainBGHeight),
            new Rectangle(GameVariables.TerrainBGWidth, 0, GameVariables.TerrainBGWidth, GameVariables.TerrainBGHeight),
            new Rectangle(GameVariables.TerrainBGWidth, GameVariables.TerrainBGHeight, GameVariables.TerrainBGWidth, GameVariables.TerrainBGHeight),
            new Rectangle(GameVariables.TerrainBGWidth * 2, 0, GameVariables.TerrainBGWidth, GameVariables.TerrainBGHeight),
            new Rectangle(GameVariables.TerrainBGWidth * 2, GameVariables.TerrainBGHeight, GameVariables.TerrainBGWidth, GameVariables.TerrainBGHeight)
        };
        private Rectangle[] _terrainFGSourceRectangle = new Rectangle[] {
            new Rectangle(0, 0, GameVariables.TerrainFGWidth, GameVariables.TerrainFGHeight),
            new Rectangle(0, GameVariables.TerrainFGHeight, GameVariables.TerrainFGWidth, GameVariables.TerrainFGHeight),
            new Rectangle(GameVariables.TerrainFGWidth, 0, GameVariables.TerrainFGWidth, GameVariables.TerrainFGHeight),
            new Rectangle(GameVariables.TerrainFGWidth, GameVariables.TerrainFGHeight, GameVariables.TerrainFGWidth, GameVariables.TerrainFGHeight),
            new Rectangle(GameVariables.TerrainFGWidth * 2, 0, GameVariables.TerrainFGWidth, GameVariables.TerrainFGHeight),
            new Rectangle(GameVariables.TerrainFGWidth * 2, GameVariables.TerrainFGHeight, GameVariables.TerrainFGWidth, GameVariables.TerrainFGHeight)
        };
        private Rectangle _terrainBGDestinationRectangle;
        private Rectangle _terrainFGDestinationRectangle;
        private TerrainTextureManager _terrainTextureManager;
        private static int NumberOfTiles = 0; // count of MapTiles on game map
        private Terrain _currentTerrain;

        public MapTile(int column, int row, TerrainType terrain, TerrainTextureManager terrainTextureManager)
        {
            _column = column;
            _row = row;
            _tileLeft = column * GameVariables.TileWidth;
            _tileBottom = (row + 1) * GameVariables.TileHeight;
            _tileCenterX = _tileLeft + GameVariables.TileWidth / 2;
            _tileUnitBottom = _tileBottom + _unitYOffset;
            _flashing = false;
            _terrainBGTop = _tileUnitBottom - GameVariables.TerrainBGHeight;
            _terrainBGDestinationRectangle = new Rectangle(_tileLeft, _terrainBGTop, GameVariables.TerrainBGWidth,
                GameVariables.TerrainBGHeight);
            int terrainFGTop = _tileBottom - GameVariables.TerrainFGHeight;
            _terrainFGDestinationRectangle = new Rectangle(_tileLeft, terrainFGTop, GameVariables.TerrainFGWidth,
                GameVariables.TerrainFGHeight);
            _terrainType = terrain;
            SetTerrainByType();
            _terrainTextureManager = terrainTextureManager;
            SetTerrainTextures();
            TileIndex = NumberOfTiles;
            NumberOfTiles++;
        }

        /// <summary>
        /// Comparison for sorting MapTile arrays by TileIndex, with the lowest TileIndex coming first in an Array.
        /// </summary>
        /// <param name="otherMapTile"></param>
        /// <returns>
        /// Value < 0: this MapTile comes first (and has a lower TileIndex)
        /// Value ==0: the two MapTiles have the same TileIndex
        /// Value > 0: this MapTile comes last (and has a higher TileIndex)
        /// </returns>
        public int CompareTo(object otherMapTile)
        {
            return this.TileIndex - ((MapTile)otherMapTile).TileIndex;
        }

        /// <summary>
        /// Comparison for sorting a MapTile List by TileIndex, with the lowest TileIndex coming first in a List.
        /// </summary>
        /// <param name="otherMapTile"></param>
        /// <returns>
        /// Value < 0: this MapTile comes first (and has a lower TileIndex)
        /// Value ==0: the two MapTiles have the same TileIndex
        /// Value > 0: this MapTile comes last (and has a higher TileIndex)
        /// </returns>
        public int CompareTo(MapTile otherMapTile)
        {
            return this.TileIndex - otherMapTile.TileIndex;
        }

        public int Column => _column;
        public int Row => _row;
        public bool Walkable => _currentTerrain.Walkable;
        public float WalkCost
        {
            get
            {
                if (Walkable)
                {
                    return ((IWalkableTerrain)_currentTerrain).WalkCost;
                }
                else
                {
                    return 100f;
                }
            }
        }
        public float AttackDamageModifier
        {
            get
            {
                if (Walkable)
                {
                    return ((IWalkableTerrain)_currentTerrain).AttackDamageModifier;
                }
                else
                {
                    return 1.0f;
                }
            }
        }
        public float DefenseDamageModifier
        {
            get
            {
                if (Walkable)
                {
                    return ((IWalkableTerrain)_currentTerrain).DefenseDamageModifier;
                }
                else
                {
                    return 1.0f;
                }
            }
        }

        public int TileIndex { get; } // index of current MapTile on game map

        /// <summary>
        /// Sets the tile counter to zero. Inteded for use when switching game maps.
        /// </summary>
        public static void ResetTileCount()
        {
            NumberOfTiles = 0;
        }

        /// <summary>
        /// Indicates whether palette animation is active
        /// </summary>
        public bool Flashing
        {
            get
            {
                return _flashing;
            }

            set
            {
                _flashing = value;
                if (value == false)
                {
                    _paletteIndex = 0;
                    _paletteCount = 0;
                    RefreshSourceFrame();
                }
            }
        }

        /// <summary>
        /// Property to indicate whether the tile is currently on fire
        /// </summary>
        public bool Burning { get; set; }

        /// <summary>
        /// The character currently on this tile
        /// </summary>
        public Character CharacterOnTile { get; set; } = null;

        /// <summary>
        /// The friendly character currently on this tile (for passing friends)
        /// </summary>
        public Character FriendOnTile { get; set; } = null;

        public Cursor CursorOnTile { get; set; } = null;

        public int UnitX
        {
            get => _tileCenterX;
        }

        /// <summary>
        /// The y-coordinate (on the map) of the bottom of units placed on this tile.
        /// </summary>
        public int UnitY
        {
            get => _tileUnitBottom;
        }

        public int TileBottom => _tileBottom;
        public int TileLeft => _tileLeft;

        /// <summary>
        /// Sets the terrain textures for the background and foreground of this tile based on the tile's
        /// current TerrainType
        /// </summary>
        private void SetTerrainTextures()
        {
            _terrainBG = _terrainTextureManager.TerrainBGTexture(_terrainType);
            _terrainFG = _terrainTextureManager.TerrainFGTexture(_terrainType);
        }

        private void SetTerrainByType()
        {
            switch (_terrainType)
            {
                case TerrainType.DryGrass:
                    _currentTerrain = new DryGrass();
                    break;
                case TerrainType.GreenGrass:
                    _currentTerrain = new GreenGrass();
                    break;
                case TerrainType.Forest:
                    _currentTerrain = new ForestTerrain();
                    break;
                case TerrainType.Water:
                    _currentTerrain = new WaterTerrain();
                    break;
            }
        }

        public void MakeDark()
        {
            _paletteIndex = 1;
            RefreshSourceFrame();
        }

        private void RefreshSourceFrame()
        {
            _sourceFrame = (_terrainFrameLast + 1) * _paletteIndex + _terrainFrame;
        }

        public void UpdateTile()
        {
            if (_terrainType == TerrainType.Water)
            {
                _terrainStepCount++;
                if (_terrainStepCount > _terrainSteps)
                {
                    _terrainStepCount = 0;
                    _terrainFrame++;
                    RefreshSourceFrame();
                    if (_terrainFrame > _terrainFrameLast)
                    {
                        _terrainFrame = 0;
                        RefreshSourceFrame();
                    }
                }
            }

            if (_flashing)
            {
                _paletteCount++;
                if (_paletteCount > _paletteSteps)
                {
                    _paletteCount = 0;
                    _paletteIndex++;
                    RefreshSourceFrame();
                    if (_paletteIndex > _paletteIndexLast)
                    {
                        _paletteIndex = 0;
                        RefreshSourceFrame();
                    }
                }
            }

            CharacterOnTile?.UpdateCharacter();
            FriendOnTile?.UpdateCharacter();
            CursorOnTile?.Update();
        }

        /// <summary>
        /// Draws the terrain background of this tile
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch to be used for drawing</param>
        /// <param name="viewManager">The ViewManager used for adjusting view</param>
        public void DrawTileBG(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            if (IsOnScreen(viewManager))
            {
                Rectangle destinationRectangle = new Rectangle();
                destinationRectangle.X = _terrainBGDestinationRectangle.X - viewManager.Left;
                destinationRectangle.Y = _terrainBGDestinationRectangle.Y - viewManager.Top;
                destinationRectangle.Width = _terrainBGDestinationRectangle.Width;
                destinationRectangle.Height = _terrainBGDestinationRectangle.Height;
                spriteBatch.Draw(_terrainBG, destinationRectangle, _terrainBGSourceRectangle[_sourceFrame], Color.White);
            }
        }

        /// <summary>
        /// Draws the terrain foreground of this tile
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch to be used for drawing</param>
        /// <param name="viewManager">The ViewManager used for adjusting view</param>
        public void DrawTileFG(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            if (IsOnScreen(viewManager))
            {
                Rectangle destinationRectangle = new Rectangle();
                destinationRectangle.X = _terrainFGDestinationRectangle.X - viewManager.Left;
                destinationRectangle.Y = _terrainFGDestinationRectangle.Y - viewManager.Top;
                destinationRectangle.Width = _terrainFGDestinationRectangle.Width;
                destinationRectangle.Height = _terrainFGDestinationRectangle.Height;
                spriteBatch.Draw(_terrainFG, destinationRectangle, _terrainFGSourceRectangle[_sourceFrame], Color.White);
            }
        }

        /// <summary>
        /// Returns whether the tile is on the screen with position given by viewManager
        /// </summary>
        /// <param name="viewManager">ViewManager to use for checking</param>
        /// <returns></returns>
        public bool IsOnScreen(ViewManager viewManager)
        {
            int viewLeft = viewManager.Left;
            int viewTop = viewManager.Top;
            int viewRight = viewManager.Right;
            int viewBottom = viewManager.Bottom;
            int tileWidth = GameVariables.TileWidth;
            bool onScreen = true;
            
            if ((_tileLeft + tileWidth) < viewLeft) { onScreen = false; }
            else if (_tileLeft > viewRight) { onScreen = false; }
            if (_tileBottom < viewTop) { onScreen = false; }
            else if (_terrainBGTop > viewBottom) { onScreen = false; }
            
            return onScreen;
        }
    }

    public enum TerrainType
    {
        None,
        DryGrass,
        GreenGrass,
        Forest,
        Water,
        Dirt
    }
}
