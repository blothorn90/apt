﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt { 
	public class TerrainTextureManager
	{
		private Dictionary<TerrainType, Texture2D> _terrainBGTextures = new Dictionary<TerrainType, Texture2D>();
        private Dictionary<TerrainType, Texture2D> _terrainFGTextures = new Dictionary<TerrainType, Texture2D>();
        public TerrainTextureManager()
		{
		}

		public void LoadTerrainTextures(Game1 game1)
		{
			_terrainBGTextures[TerrainType.DryGrass] = game1.Content.Load<Texture2D>("dryGrassBGStrip3");
            _terrainFGTextures[TerrainType.DryGrass] = game1.Content.Load<Texture2D>("dryGrassFGStrip3");
            _terrainBGTextures[TerrainType.GreenGrass] = game1.Content.Load<Texture2D>("greenGrassBGStrip3");
            _terrainFGTextures[TerrainType.GreenGrass] = game1.Content.Load<Texture2D>("greenGrassFGStrip3");
            _terrainBGTextures[TerrainType.Forest] = game1.Content.Load<Texture2D>("forestBGStrip3");
            _terrainFGTextures[TerrainType.Forest] = game1.Content.Load<Texture2D>("forestFGStrip3");
            _terrainBGTextures[TerrainType.Water] = game1.Content.Load<Texture2D>("waterBGStrip3");
            _terrainFGTextures[TerrainType.Water] = game1.Content.Load<Texture2D>("waterFGStrip3");
        }

		public Texture2D TerrainBGTexture(TerrainType terrainType)
		{
			return _terrainBGTextures[terrainType];
        }

        public Texture2D TerrainFGTexture(TerrainType terrainType)
        {
            return _terrainFGTextures[terrainType];
        }
    }
}