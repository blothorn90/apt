﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt
{
    public class GuiManager
    {
        private Game1 _game1;
        private FontManager _fontManager;
        private int _guiWidth;
        private int _guiHeight;
        private int _windowGapWidth = 4; // width in pixels of gap between windows
        private int _textGapWidth = 1; // width in pixels of gap between window sprite corners and text
        private int _textHeight = 20; // height of text characters in pixels
        private int _textLineHeight;
        private int _textboxLines = 2; // number of lines of text in a textbox
        private int _windowSpriteWidth = 48; // width of window sprite in pixels
        private int _windowSpriteHeight = 48;
        private int _windowSpriteCorner = 12; // length of window sprite corner decoration in pixels
        private int _windowSpriteSide = 24; // length of side section of window sprite (section without corners)
        private int _numberOfColumns; // columns in choice menu
        private int _numberOfRows; // rows in choice menu
        private int _columnGap = 18; // space between columns
        private int _widthOfChoices; // width of choices including buttons but not window
        private int _choiceWindowWidth;
        private int _choiceWindowHeight;
        private int _activeChoice = 0; // index of the active selection
        private bool _executingOption = false; // whether selected menu choice is being executed
        private static readonly int _fps = 75; // frames per second for choice timer
        private static readonly int _totalExecutionTime = _fps * 5; // frames for executing choice
        private int _executionCount = 0; // choice execution timer
        private Texture2D _windowTexture;
        private Texture2D _buttonTexture;
        private Rectangle _windowSourceTopLeftCorner;
        private Rectangle _windowSourceTopRightCorner;
        private Rectangle _windowSourceBottomLeftCorner;
        private Rectangle _windowSourceBottomRightCorner;
        private Rectangle _windowSourceTopSide;
        private Rectangle _windowSourceLeftSide;
        private Rectangle _windowSourceBottomSide;
        private Rectangle _windowSourceRightSide;
        private Rectangle _windowSourceCenter;
        private Rectangle _windowDestinationTopLeftCorner;
        private Rectangle _windowDestinationTopRightCorner;
        private Rectangle _windowDestinationBottomLeftCorner;
        private Rectangle _windowDestinationBottomRightCorner;
        private Rectangle _windowDestinationTopSide;
        private Rectangle _windowDestinationLeftSide;
        private Rectangle _windowDestinationBottomSide;
        private Rectangle _windowDestinationRightSide;
        private Rectangle _windowDestinationCenter;
        private List<MenuOption> _menuOptions = new List<MenuOption>();
        private bool _optionsReady = false;
        private bool _acceptingInput = false;

        public GuiManager(Game1 game1, FontManager fontManager) {
            _game1 = game1;
            _fontManager = fontManager;
            _guiWidth = _game1._guiWidth;
            _guiHeight = _game1._guiHeight;
            _textLineHeight = _textHeight + 2; // add a pixel for shadow and a pixel for space
            SetupSourceRectangles();
        }
        // whether the window can currently be controlled by player input
        public bool WindowActive { get; set; } = false;
        public bool AcceptingInput => _acceptingInput;
        private void SetupSourceRectangles()
        {
            // corners
            _windowSourceTopLeftCorner = new Rectangle(0, 0, _windowSpriteCorner, _windowSpriteCorner);
            _windowSourceTopRightCorner = new Rectangle(_windowSpriteWidth - _windowSpriteCorner, 0, _windowSpriteCorner, _windowSpriteCorner);
            _windowSourceBottomLeftCorner = new Rectangle(0, _windowSpriteHeight - _windowSpriteCorner, _windowSpriteCorner, _windowSpriteCorner);
            _windowSourceBottomRightCorner = new Rectangle(_windowSpriteWidth - _windowSpriteCorner, _windowSpriteHeight - _windowSpriteCorner,
                _windowSpriteCorner, _windowSpriteCorner);

            // sides
            _windowSourceTopSide = new Rectangle(_windowSpriteCorner, 0, _windowSpriteSide, _windowSpriteCorner);
            _windowSourceLeftSide = new Rectangle(0, _windowSpriteCorner, _windowSpriteCorner, _windowSpriteSide);
            _windowSourceBottomSide = new Rectangle(_windowSpriteCorner, _windowSpriteHeight - _windowSpriteCorner, _windowSpriteSide, _windowSpriteCorner);
            _windowSourceRightSide = new Rectangle(_windowSpriteWidth - _windowSpriteCorner, _windowSpriteCorner, _windowSpriteCorner, _windowSpriteSide);

            // center
            _windowSourceCenter = new Rectangle(_windowSpriteCorner, _windowSpriteCorner, _windowSpriteSide, _windowSpriteSide);
        }
        private void SetupDestinationRectangles(int x, int y, int width, int height)
        {
            int right = x + width;
            int bottom = y + height;
            int sideWidth = width - (2 * _windowSpriteCorner);
            int sideHeight = height - (2 * _windowSpriteCorner);

            // corners
            _windowDestinationTopLeftCorner = new Rectangle(x, y, _windowSpriteCorner, _windowSpriteCorner);
            _windowDestinationTopRightCorner = new Rectangle(right - _windowSpriteCorner, y, _windowSpriteCorner, _windowSpriteCorner);
            _windowDestinationBottomLeftCorner = new Rectangle(x, bottom - _windowSpriteCorner, _windowSpriteCorner, _windowSpriteCorner);
            _windowDestinationBottomRightCorner = new Rectangle(right - _windowSpriteCorner, bottom - _windowSpriteCorner,
                _windowSpriteCorner, _windowSpriteCorner);

            // sides
            _windowDestinationTopSide = new Rectangle(x + _windowSpriteCorner, y, sideWidth, _windowSpriteCorner);
            _windowDestinationLeftSide = new Rectangle(x, y + _windowSpriteCorner, _windowSpriteCorner, sideHeight);
            _windowDestinationBottomSide = new Rectangle(x + _windowSpriteCorner, bottom - _windowSpriteCorner, sideWidth, _windowSpriteCorner);
            _windowDestinationRightSide = new Rectangle(right - _windowSpriteCorner, y + _windowSpriteCorner, _windowSpriteCorner, sideHeight);

            // center
            _windowDestinationCenter = new Rectangle(x + _windowSpriteCorner, y + _windowSpriteCorner, sideWidth, sideHeight);
        }
        public void LoadGuiTextures(Game1 game1)
        {
            _windowTexture = game1.Content.Load<Texture2D>("windowBack");
            _buttonTexture = game1.Content.Load<Texture2D>("button");
        }
        public void DrawGui(SpriteBatch spriteBatch)
        {
            /* Test font
            string testString = "The 12345 quick brown foxes jumped over the 67890 lazy dogs?!.";
            Vector2 textPosition = new Vector2(10, 10);
            spriteBatch.DrawString(_fontManager.CurrentFont, testString, textPosition, Color.White);
            */
            /*
            // test window
            string message = "The -12,345 \"quick\" Dr. Brown's foxes jumped over the +67,890 lazy dogs?!";
            Write(spriteBatch, message);
            */
            // Test choice window
            if (WindowActive)
            {
                DrawWindow(spriteBatch, 0, 0, _choiceWindowWidth, _choiceWindowHeight);
                if (!_executingOption)
                {
                    foreach (MenuOption option in _menuOptions)
                    {
                        option.Draw(spriteBatch);
                    }
                }
                else
                {
                    _menuOptions[_activeChoice].Draw(spriteBatch);
                }
            }
        }
        public void Write(SpriteBatch spriteBatch, string text)
        {
            int xWindow = _windowGapWidth;
            int xText = xWindow + _windowSpriteCorner + _textGapWidth;
            int windowHeight = (2 * _windowSpriteCorner) + (_textboxLines * _textLineHeight) + _textGapWidth;
            int windowWidth = _guiWidth - (2 * _windowGapWidth);
            int yWindow = _guiHeight - _windowGapWidth - windowHeight;
            int yText = yWindow + _windowSpriteCorner + _textGapWidth;
            DrawWindow(spriteBatch, xWindow, yWindow, windowWidth, windowHeight);
            DrawText(spriteBatch, text, xText, yText);
        }
        /// <summary>
        /// This method sets up a choice window with a maximum of 8 choices.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="choiceStrings"></param>
        public void SetupChoiceWindow(List<string> choiceStrings)
        {
            int column1Width = 0;
            int column2Width = 0;
            int choiceWidth = 0;
            int numberOfChoices = choiceStrings.Count;
            int numberOfColumns = 1;
            int numberOfRows = numberOfChoices;
            if (numberOfChoices > 4)
            {
                numberOfColumns = 2;
                if (numberOfChoices > 6)
                {
                    numberOfRows = 4;
                }
                else
                {
                    numberOfRows = 3;
                }
            }
            _numberOfColumns = numberOfColumns;
            _numberOfRows = numberOfRows;

            // Setup column 1
            int xColumn1 = _windowSpriteCorner + _textGapWidth;
            int yOption = xColumn1;
            _menuOptions.Add(new MenuOption(_buttonTexture, _fontManager.CurrentFont, choiceStrings[0], xColumn1, yOption, true));
            column1Width = _menuOptions[0].Width;
            for (int i = 1; i < numberOfRows; i++)
            {
                yOption += _textLineHeight;
                _menuOptions.Add(new MenuOption(_buttonTexture, _fontManager.CurrentFont, choiceStrings[i], xColumn1, yOption));
                choiceWidth = _menuOptions[i].Width;
                if (choiceWidth > column1Width)
                {
                    column1Width = choiceWidth;
                }
            }

            if (numberOfColumns > 1)
            {
                // Setup column 2
                int xColumn2 = xColumn1 + column1Width + _columnGap;
                yOption = xColumn1;
                choiceWidth = 0;
                for (int i = numberOfRows; i < numberOfChoices; i++)
                {
                    _menuOptions.Add(new MenuOption(_buttonTexture, _fontManager.CurrentFont, choiceStrings[i], xColumn2, yOption));
                    choiceWidth = _menuOptions[i].Width;
                    if (choiceWidth > column2Width)
                    {
                        column2Width = choiceWidth;
                    }
                    yOption += _textLineHeight;
                }
            }
            if (numberOfColumns > 1)
            {
                _widthOfChoices = column1Width + _columnGap + column2Width;
            }
            else
            {
                _widthOfChoices = column1Width;
            }
            _choiceWindowWidth = _widthOfChoices + (2 * _textGapWidth) + (2 * _windowSpriteCorner);
            _choiceWindowHeight = (_numberOfRows * _textLineHeight) + _textGapWidth + (2 * _windowSpriteCorner);
            _optionsReady = true;
        }
        /// <summary>
        /// Moves the current selection in the choice window according to the input direction.
        /// character parameter is not currently used.
        /// </summary>
        /// <param name="character"></param>
        /// <param name="direction"></param>
        public void MoveSelection(Character character, MoveDirection direction)
        {
            bool selectionChanged = true;
            if (_numberOfColumns > 1)
            {
                switch (direction)
                {
                    case MoveDirection.Left:
                        _activeChoice -= _numberOfRows;
                        break;
                    case MoveDirection.Right:
                        _activeChoice += _numberOfRows;
                        break;
                    case MoveDirection.Up:
                        _activeChoice--;
                        break;
                    case MoveDirection.Down:
                        _activeChoice++;
                        break;
                    default:
                        selectionChanged = false;
                        break;
                }
            }
            else
            {
                switch (direction)
                {
                    case MoveDirection.Left:
                    case MoveDirection.Up:
                        _activeChoice--;
                        break;
                    case MoveDirection.Right:
                    case MoveDirection.Down:
                        _activeChoice++;
                        break;
                    default:
                        selectionChanged = false;
                        break;
                }
            }
            if (selectionChanged)
            {
                // keep selection in range of choices
                if (_activeChoice < 0)
                {
                    _activeChoice += _menuOptions.Count;
                }
                else if (_activeChoice >= _menuOptions.Count)
                {
                    _activeChoice -= _menuOptions.Count;
                }
                // update List with new choice
                for (int i = 0; i < _menuOptions.Count; i++)
                {
                    if (i == _activeChoice)
                    {
                        _menuOptions[i].Activate();
                    }
                    else
                    {
                        _menuOptions[i].Deactivate();
                    }
                }
            }
        }
        /// <summary>
        /// Closes the active window. character parameter is not currently used.
        /// </summary>
        /// <param name="character"></param>
        public void CloseMenu(Character character)
        {
            WindowActive = false;
            _acceptingInput = false;
        }
        /// <summary>
        /// Opens the main menu. character parameter is not currently used.
        /// </summary>
        /// <param name="character"></param>
        public void OpenMenu(Character character)
        {
            WindowActive = true;
            _acceptingInput = true;
        }
        public void ExecuteOption(Character character)
        {
            _executingOption = true;
            _executionCount = _totalExecutionTime;
            _acceptingInput = false;
            _menuOptions[_activeChoice].IsLit = true;
        }
        private void DrawWindow(SpriteBatch spriteBatch, int x, int y, int width, int height)
        {
            SetupDestinationRectangles(x, y, width, height);

            // corners
            spriteBatch.Draw(_windowTexture, _windowDestinationTopLeftCorner, _windowSourceTopLeftCorner, Color.White);
            spriteBatch.Draw(_windowTexture, _windowDestinationTopRightCorner, _windowSourceTopRightCorner, Color.White);
            spriteBatch.Draw(_windowTexture, _windowDestinationBottomLeftCorner, _windowSourceBottomLeftCorner, Color.White);
            spriteBatch.Draw(_windowTexture, _windowDestinationBottomRightCorner, _windowSourceBottomRightCorner, Color.White);

            // sides
            spriteBatch.Draw(_windowTexture, _windowDestinationTopSide, _windowSourceTopSide, Color.White);
            spriteBatch.Draw(_windowTexture, _windowDestinationLeftSide, _windowSourceLeftSide, Color.White);
            spriteBatch.Draw(_windowTexture, _windowDestinationBottomSide, _windowSourceBottomSide, Color.White);
            spriteBatch.Draw(_windowTexture, _windowDestinationRightSide, _windowSourceRightSide, Color.White);

            // center
            spriteBatch.Draw(_windowTexture, _windowDestinationCenter, _windowSourceCenter, Color.White);
        }
        private void DrawText(SpriteBatch spriteBatch, string text, int x, int y)
        {
            Vector2 shadowRightPosition = new Vector2(x + 1, y);
            Vector2 shadowBottomPosition = new Vector2(x, y + 1);
            Vector2 textPosition = new Vector2(x, y);
            spriteBatch.DrawString(_fontManager.CurrentFont, text, shadowRightPosition, Color.Black);
            spriteBatch.DrawString(_fontManager.CurrentFont, text, shadowBottomPosition, Color.Black);
            spriteBatch.DrawString(_fontManager.CurrentFont, text, textPosition, Color.White);
        }
        public void Update()
        {
            if (WindowActive)
            {
                if (!_optionsReady)
                {
                    // test choice menu 2
                    List<string> testChoices = new List<string>();
                    testChoices.Add("Choice 1");
                    testChoices.Add("c2");
                    testChoices.Add("Long Choice 3");
                    testChoices.Add("Choice 4");
                    testChoices.Add("s5");
                    testChoices.Add("s6");
                    testChoices.Add("Choice 7");
                    testChoices.Add("The longest of choices 008.");
                    /*
                    */
                    SetupChoiceWindow(testChoices);
                    // Test choice menu
                    /*
                    string option1 = "Active Demo";
                    string option2 = "Inactive Demo";
                    int xOptions = _windowSpriteCorner + _textGapWidth;
                    int yOption1 = xOptions;
                    int yOption2 = yOption1 + _textLineHeight;
                    _menuOptions.Add(new MenuOption(_buttonTexture, _fontManager.CurrentFont, option1, xOptions, yOption1, true));
                    _menuOptions.Add(new MenuOption(_buttonTexture, _fontManager.CurrentFont, option2, xOptions, yOption2));
                    _optionsReady = true;
                    */
                    // end of choice menu test code
                }
                else
                {
                    if (!_executingOption)
                    {
                        foreach (MenuOption option in _menuOptions)
                        {
                            option.Update();
                        }
                    }
                    else
                    {
                        _executionCount--;
                        if (_executionCount == 0)
                        {
                            _executingOption = false;
                            WindowActive = false;
                        }
                    }
                }
            }
        }
    }
}
