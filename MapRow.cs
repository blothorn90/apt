﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt
{
    public class MapRow
    {
        private MapTile[] _mapTiles;
        public MapRow(int numberOfTiles)
        {
            _mapTiles = new MapTile[numberOfTiles];
        }

        public MapTile this [int column]
        {
            get
            {
                if ((column >=0) && (column < _mapTiles.Length))
                {
                    return _mapTiles[column];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if ((column >= 0) && (column < _mapTiles.Length))
                {
                    _mapTiles[column] = value;
                }
            }
        }

        public void UpdateRow()
        {
            for (int i = 0; i < _mapTiles.Length; i++)
            {
                _mapTiles[i].UpdateTile();
            }
        }

        /// <summary>
        /// Draws the terrain background for all tiles in this row
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch used for drawing</param>
        /// <param name="viewManager">The ViewManager used for adjusting view</param>
        public void DrawRowBG(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            for (int i = 0;  i < _mapTiles.Length; i++)
            {
                _mapTiles[i].DrawTileBG(spriteBatch, viewManager);
                _mapTiles[i].CursorOnTile?.DrawCursorBG(spriteBatch, viewManager);
            }
        }

        /// <summary>
        /// Draws the terrain foreground for all tiles in this row
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch used for drawing</param>
        /// <param name="viewManager">The ViewManager used for adjusting view</param>
        public void DrawRowFG(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            for (int i = 0; i < _mapTiles.Length; i++)
            {
                _mapTiles[i].DrawTileFG(spriteBatch, viewManager);
                _mapTiles[i].CursorOnTile?.DrawCursorFG(spriteBatch, viewManager);
            }
        }

        public void DrawRowCharacters(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            int friendY;
            int charY;

            for (int i = 0; i < _mapTiles.Length; i++)
            {
                friendY = _mapTiles[i].FriendOnTile?.Y ?? -5;
                charY = _mapTiles[i].CharacterOnTile?.Y ?? -5;
                if (friendY < charY)
                {
                    _mapTiles[i].FriendOnTile?.DrawCharacter(spriteBatch, viewManager);
                    _mapTiles[i].CharacterOnTile?.DrawCharacter(spriteBatch, viewManager);
                }
                else
                {
                    _mapTiles[i].CharacterOnTile?.DrawCharacter(spriteBatch, viewManager);
                    _mapTiles[i].FriendOnTile?.DrawCharacter(spriteBatch, viewManager);
                }
            }
        }

        /// <summary>
        /// Draws the terrain background and foreground of each tile in this row
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch used for drawing</param>
        /// <param name="viewManager">The ViewManager used for adjusting view</param>
        public void DrawRowComplete(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            DrawRowBG(spriteBatch, viewManager);
            DrawRowCharacters(spriteBatch, viewManager);
            DrawRowFG(spriteBatch, viewManager);
        }
    }
}
