﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace apt
{
    public class MapReader
    {
        private string[] _mapStrings;
        private int _rowWidth; // number of columns in a row
        private int _mapHeight; // number of rows on map
        private MapRow[] _mapRows;
        private Dictionary<int, TerrainType> _terrainIndexDictionary = new Dictionary<int, TerrainType>();
        private Dictionary<int, CharacterName> _characterDictionary = new Dictionary<int, CharacterName>();
        private TerrainTextureManager _terrainTextureManager;
        private SpriteTextureManager _spriteTextureManager;
        private InputManager _inputManager;
        private ViewManager _viewManager;
        private List<Character> _allCharacters = new List<Character>();
        private List<MapTile> _allTiles = new List<MapTile>();

        public List<Character> CharacterList
        {
            get
            {
                return _allCharacters;
            }
        }
        public List<MapTile> AllTiles => _allTiles;
        public int NumberOfColumns => _rowWidth;
        public int NumberOfRows => _mapHeight;
        public int MapRightEdge => NumberOfColumns * GameVariables.TileWidth;
        public int MapBottomEdge => NumberOfRows * GameVariables.TileHeight;
        
        public MapReader(TerrainTextureManager terrainTextureManager,
            SpriteTextureManager spriteTextureManager,
            InputManager inputManager,
            ViewManager viewManager)
        {
            _mapStrings = File.ReadAllLines(@"Content\testWorld.tmx");
            // _mapStrings = File.ReadAllLines(@"..\..\..\Content\testWorld.tmx");
            _terrainTextureManager = terrainTextureManager;
            _spriteTextureManager = spriteTextureManager;
            ExtractRowWidth();
            ExtractMapHeight();
            _terrainIndexDictionary[2] = TerrainType.DryGrass;
            _terrainIndexDictionary[3] = TerrainType.GreenGrass;
            _terrainIndexDictionary[4] = TerrainType.Forest;
            _terrainIndexDictionary[5] = TerrainType.Water;
            _characterDictionary[0] = CharacterName.None;
            _characterDictionary[38] = CharacterName.KungFu;
            _characterDictionary[39] = CharacterName.Temp2;
            _characterDictionary[40] = CharacterName.Temp3;
            _characterDictionary[41] = CharacterName.Temp4;
            _characterDictionary[44] = CharacterName.Wahsk;
            _inputManager = inputManager;
            _viewManager = viewManager;
            _viewManager.SetMapReader(this);
            CreateMapRows();
            ExtractTerrainData();
            ExtractCharacterData();
        }

        private void ExtractRowWidth()
        {
            string lookingFor = "\\bwidth=\"(?<widthValueText>\\d+)\""; // regular expression
            Match widthTextMatch = Regex.Match(_mapStrings[1], lookingFor);
            _rowWidth = Convert.ToInt32(widthTextMatch.Groups["widthValueText"].Value);
        }
        private void ExtractMapHeight()
        {
            string lookingFor = "\\bheight=\"(?<heightValueText>\\d+)\""; // regular expression
            Match heightTextMatch = Regex.Match(_mapStrings[1], lookingFor);
            _mapHeight = Convert.ToInt32(heightTextMatch.Groups["heightValueText"].Value);
        }

        private void CreateMapRows()
        {
            _mapRows = new MapRow[_mapHeight];
        }

        private void ExtractTerrainData()
        {
            int datumRowOffset = 6; // offset of starting terrain data line in rows from top
            string[] terrainTexts;
            int currentDatum;
            TerrainType currentTerrain;
            MapTile mapTile;
            for (int i = 0; i < _mapRows.Length; i++)
            {
                _mapRows[i] = new MapRow(_rowWidth);
                terrainTexts = _mapStrings[i + datumRowOffset].TrimEnd(',').Split(',');
                for (int j = 0; j < _rowWidth; j++)
                {
                    currentDatum = Convert.ToInt32(terrainTexts[j]);
                    currentTerrain = _terrainIndexDictionary[currentDatum];
                    mapTile = new MapTile(j, i, currentTerrain, _terrainTextureManager);
                    _mapRows[i][j] = mapTile;
                    _allTiles.Add(mapTile);
                }
            }
        }

        private void ExtractCharacterData()
        {
            int datumRowOffset = 10 + _mapHeight; // offset of starting character data line in rows from top
            string[] characterTexts;
            CharacterName currentCharacter;
            int currentDatum;
            for (int i = 0; i < _mapRows.Length; i++)
            {
                characterTexts = _mapStrings[i + datumRowOffset].TrimEnd(',').Split(',');
                for (int j = 0; j < _rowWidth; j++)
                {
                    currentDatum = Convert.ToInt32(characterTexts[j]);
                    currentCharacter = _characterDictionary[currentDatum];
                    if (currentCharacter != CharacterName.None)
                    {
                        switch (currentCharacter)
                        {
                            case CharacterName.Wahsk:
                                _allCharacters.Add(new Wahsk(_spriteTextureManager, _mapRows[i][j], _inputManager, currentCharacter));
                                break;
                            default:
                                _allCharacters.Add(new Character(_spriteTextureManager, _mapRows[i][j], _inputManager, currentCharacter));
                                break;
                        }
                    }
                }
            }
            _allCharacters.Sort();
        }

        public MapRow GetMapRow(MapTile mapTile)
        {
            return _mapRows[mapTile.Row];
        }

        /// <summary>
        /// Used to get the MapTile to the right of the supplied MapTile
        /// </summary>
        /// <param name="mapTile"></param>
        /// <returns>
        /// MapTile adjacent to the right of the supplied MapTile if available.
        /// Otherwise, returns the supplied MapTile.
        /// </returns>
        public MapTile GetTileOnRight(MapTile mapTile)
        {
            int currentRow = mapTile.Row;
            int currentColumn = mapTile.Column;
            if (currentColumn == (_rowWidth - 1))
            {
                return mapTile;
            }
            else
            {
                return _mapRows[currentRow][currentColumn + 1];
            }
        }
        public MapTile GetTileOnLeft(MapTile mapTile)
        {
            int currentRow = mapTile.Row;
            int currentColumn = mapTile.Column;
            if (currentColumn == 0)
            {
                return mapTile;
            }
            else
            {
                return _mapRows[currentRow][currentColumn - 1];
            }
        }
        public MapTile GetTileOnTop(MapTile mapTile)
        {
            int currentRow = mapTile.Row;
            int currentColumn = mapTile.Column;
            if (currentRow == 0)
            {
                return mapTile;
            }
            else
            {
                return _mapRows[currentRow - 1][currentColumn];
            }
        }
        public MapTile GetTileOnBottom(MapTile mapTile)
        {
            int currentRow = mapTile.Row;
            int currentColumn = mapTile.Column;
            if (currentRow == (_mapHeight - 1))
            {
                return mapTile;
            }
            else
            {
                return _mapRows[currentRow + 1][currentColumn];
            }
        }

        public void UpdateMap()
        {
            for (int i = 0; i < _mapRows.Length; i++)
            {
                _mapRows[i].UpdateRow();
            }
            _viewManager.Update();
        }

        /// <summary>
        /// Draws the terrain background and foreground of each tile of each row
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch used for drawing</param>
        public void DrawMap(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < _mapRows.Length; i++)
            {
                _mapRows[i].DrawRowComplete(spriteBatch, _viewManager);
            }
        }
    }

}