﻿using System;
using System.Collections.Generic;

namespace apt
{
    public abstract class Terrain
    {
        public abstract bool Walkable { get; }
        public abstract bool CanBurn { get; }
    }

    public class WaterTerrain : Terrain
    {
        public override bool Walkable => false;
        public override bool CanBurn => false;
    }

    public abstract class BurnableWalkableTerrain : Terrain, IWalkableTerrain, IBurnableTerrain
    {
        protected int _fireResistance;
        protected int _fireRemaining;

        public override bool Walkable => true;
        public override bool CanBurn => true;
        public virtual float WalkCost => 1.0f;
        public virtual float DefenseDamageModifier => 1.0f;
        public virtual float AttackDamageModifier => 1.0f;
        public virtual int InitialFireResistance => 1;
        public int FireResistance
        {
            get
            {
                return _fireResistance;
            }
            set
            {
                _fireResistance = value;
            }
        }
        public virtual int InitialFireDuration => 2;
        public int FireRemaining
        {
            get
            {
                return _fireRemaining;
            }
            set
            {
                _fireRemaining = value;
            }
        }
        public virtual TerrainType BurnsToTerrain => TerrainType.Dirt;

        public BurnableWalkableTerrain()
        {
            FireResistance = InitialFireResistance;
            FireRemaining = InitialFireDuration;
        }
    }

    public class DryGrass : BurnableWalkableTerrain
    {
    }

    public class GreenGrass : DryGrass
    {
        public override int InitialFireResistance => 2;
        public override int InitialFireDuration => 4;
    }

    public class ForestTerrain : BurnableWalkableTerrain
    {
        public override float WalkCost => 1.6f;
        public override float DefenseDamageModifier => 0.5f;
        public override int InitialFireResistance => 3;
        public override int InitialFireDuration => 8;
    }

    public interface IWalkableTerrain
    {
        float WalkCost { get; }
        float DefenseDamageModifier { get; }
        float AttackDamageModifier { get; }
    }

    public interface IBurnableTerrain
    {
        int InitialFireResistance { get; }
        int FireResistance { get; set; }

        // number of turns fire will burn
        int InitialFireDuration { get; }

        // number of turns remaining for fire to burn
        int FireRemaining { get; set; }
        TerrainType BurnsToTerrain { get; }
    }
}
