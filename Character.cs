﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace apt
{
    public class Character : IComparable, IComparable<Character>
    {
        private Texture2D _sprite;
        private Texture2D _teamTexture;
        private Texture2D _gaugeTexture;
        private int _spriteBottom;
        private int _spriteCenterX;
        private int _spriteFrame = 0; // index of sprite frame for animation
        private int _spriteFrameStep = 0; // current frame step in current sprite direction animation
        private int _spriteFrameLength = 1; // number of frames in current sprite direction animation
        private readonly static int _movingStepSpeedup = 2; // reduction of game-step delay (per game step) between frames when IsMoving
        private readonly static int _animationChangeDelay = 1; // number of game frames to wait before switching animation set
        private int _animationChangeCount = 0; // counter for animationChangeDelay
        private bool _needsStandAnimation = false; // flag for calling animation change from walk to stand
        private int _spriteStepCount = 0; // counter for sprite animation
        private Rectangle[] _sourceRectangle;
        private Rectangle _destinationRectangle;
        private Rectangle _teamSourceRectangle;
        private Rectangle _teamDestinationRectangle;
        private Rectangle _gaugeWhite;
        private Rectangle _gaugeBlack;
        private Rectangle _gaugeHPColor;
        private Rectangle _gaugeMPColor;
        private Rectangle _gaugeWhiteHPWidthRectangle;
        private Rectangle _gaugeWhiteHPHeightRectangle;
        private Rectangle _gaugeWhiteMPWidthRectangle;
        private Rectangle _gaugeWhiteMPHeightRectangle;
        private Rectangle _gaugeBlackHPRectangle;
        private Rectangle _gaugeBlackMPRectangle;
        private Rectangle _gaugeHPRectangle;
        private Rectangle _gaugeMPRectangle;
        private int _gaugeWidth = 7; // width of full HP/MP gauge in pixels
        private int _gaugeMaxHeight = 44;
        private int _gaugeYOffset = -4;
        private MapTile _mapTile;
        private MapTile _newTile;
        private SpriteTextureManager _spriteTextureManager;
        private InputManager _inputManager;
        private int _width;
        private int _height;
        private int _x; // x-coordinate of left of character
        private int _y; // y-coordinate of top of character
        private int _newX; // x-coordinate to move to
        private int _newY; // y-coordinate to move to
        private int _teamXOffset; // amount to offset team icon from sprite
        private readonly static int _moveSteps = 16; // game frames to move to _newTile
        private readonly static int _tileSwitchSteps = _moveSteps / 2; // game frames before switching _mapTile during movement
        private readonly static int _stepSize = GameVariables.TileWidth / _moveSteps; // sprite movement rate
        private int _moveStepCount = 0; // counter for tile movement animation
        private Stats _stats = new Stats();
        private int _currentSpeed; // variable based on speed stat and random numbers for determining turn order
        private static Random NumberGenerator = new Random();
        private int _level = 1;
        private bool _isMoving = false;
        private bool _needsTileSwitch = false; // whether the character needs to be moved to a new MapTile
        private MoveDirection _facingDirection;
        protected FrameOrder _frameOrder = new FrameOrder();
        private AnimationSet _animationSet;
        private List<int> _frameList;
        private CharacterName _characterName;
        protected TeamName _team;
        protected static int LastCharacterID = 0;
        protected int _hp;
        protected int _mp;

        public Character(SpriteTextureManager spriteTextureManager, MapTile mapTile, InputManager inputManager, CharacterName characterName)
        {
            _spriteTextureManager = spriteTextureManager;
            _characterName = characterName;
            _sprite = _spriteTextureManager.SpriteTexture(_characterName);
            _teamTexture = _spriteTextureManager.TeamTexture;
            _gaugeTexture = _spriteTextureManager.GaugeTexture;
            _width = _sprite.Width / (TotalSpriteFrames);
            _height = _sprite.Height;
            _spriteBottom = _height;
            _spriteCenterX = _width / 2;
            SetupSourceRectangles();
            FacingDirection = MoveDirection.Down;
            CurrentAnimation = AnimationSet.stand;
            _mapTile = mapTile;
            mapTile.CharacterOnTile = this;
            _x = _mapTile.UnitX - _spriteCenterX;
            _y = _mapTile.UnitY - _spriteBottom;
            _newX = _x;
            _newY = _y;
            _destinationRectangle = new Rectangle(_x, _y, _width, _height);
            _inputManager = inputManager;
            SetExploreAgent();
            BattleAgent = _inputManager.GetAgent(AgentName.Player1);
            SetTeam();
            SetupTeamRectangles();
            UpdateSpeed();
            MoveRemaining = MoveRange;
            HP = MaxHP;
            MP = MaxMP;
            SetupGaugeRectangles();
            CharacterID = LastCharacterID;
            LastCharacterID++;
        }

        public int CharacterID { get; }
        protected virtual int SpriteSteps => 40; // game steps between sprite frames
        protected virtual int TotalSpriteFrames { get; } = 18; // total number of sprite frames
        public Agent BattleAgent { get; set; }
        public Agent ExploreAgent { get; set; }
        public virtual float MoveRange => 6.0f;
        public float MoveRemaining { get; set; }
        public int Level => _level;
        public int HP
        {
            get
            {
                return _hp;
            }
            set
            {
                _hp = value;
                SetupGaugeRectangles();
            }
        }
        public int MP
        {
            get => _mp;
            set
            {
                _mp = value;
                SetupGaugeRectangles();
            }
        }
        public int MaxHP
        {
            get
            {
                return _stats.MaxHP;
            }
            set
            {
                _stats.MaxHP = value;
            }
        }
        public int MaxMP
        {
            get
            {
                return _stats.MaxMP;
            }
            set
            {
                _stats.MaxMP = value;
            }
        }
        public int Speed
        {
            get
            {
                return _stats.Speed;
            }
            set
            {
                _stats.Speed = value;
                UpdateSpeed();
            }
        }
        public int Attack
        {
            get
            {
                return _stats.Attack;
            }
            set
            {
                _stats.Attack = value;
            }
        }
        public int Defense
        {
            get
            {
                return _stats.Defense;
            }
            set
            {
                _stats.Defense = value;
            }
        }
        public int CurrentSpeed
        {
            get => _currentSpeed;
            set => _currentSpeed = value;
        }
        public MapTile CurrentTile
        {
            get
            {
                return _mapTile;
            }
            set
            {
                _newTile = value;
                _newX = _newTile.UnitX - _spriteCenterX;
                _newY = _newTile.UnitY - _spriteBottom;
                IsMoving = true;
                _needsTileSwitch = true;
            }
        }
        public TeamName Team
        {
            get
            {
                return _team;
            }
        }
        public virtual bool CanFly => false;
        // whether to switch MapTile quickly (for moving down)
        public bool SwitchTileFast { get; set; } = false;
        // whether to switch MapTile slowly (for moving up)
        public bool SwitchTileSlow { get; set; } = false;
        public bool IsMoving
        {
            get
            {
                return _isMoving;
            }
            set
            {
                _isMoving = value;
                if (value == false)
                {
                    _needsStandAnimation = true;
                    _animationChangeCount = 0;
                }
                else
                {
                    _needsStandAnimation = false;
                    CurrentAnimation = AnimationSet.walk;
                }
            }
        }
        public int Y
        {
            get
            {
                return _y;
            }
        }
        public int CenterX => _x + _spriteCenterX;
        public int CenterY => _y + (_height / 2);
        public MoveDirection FacingDirection
        {
            get
            {
                return _facingDirection;
            }
            set
            {
                _facingDirection = value;
                ResetSpriteFrameCounters();
            }
        }
        public SpriteTextureManager TextureManager => _spriteTextureManager;
        public virtual string ScreenName => "Fighter";
        private AnimationSet CurrentAnimation
        {
            get
            {
                return _animationSet;
            }
            set
            {
                _animationSet = value;
                ResetSpriteFrameCounters();
            }
        }

        /// <summary>
        /// Comparison for sorting Character arrays by CurrentSpeed, with the highest speed coming first in an Array.
        /// </summary>
        /// <param name="otherCharacter"></param>
        /// <returns>
        /// Value < 0: this character comes first (and is faster)
        /// Value ==0: the two characters have the same speed
        /// Value > 0: this character comes last (and is slower)
        /// </returns>
        public int CompareTo(object otherCharacter)
        {
            return ((Character)otherCharacter).CurrentSpeed - this.CurrentSpeed;
        }

        /// <summary>
        /// Comparison for sorting a List by CurrentSpeed, with the highest speed coming first in a List.
        /// </summary>
        /// <param name="otherCharacter"></param>
        /// <returns>
        /// Value < 0: this character comes first (and is faster)
        /// Value ==0: the two characters have the same speed
        /// Value > 0: this character comes last (and is slower)
        /// </returns>
        public int CompareTo(Character otherCharacter)
        {
            return otherCharacter.CurrentSpeed - this.CurrentSpeed;
        }

        private void SetupSourceRectangles()
        {
            int x;
            int width = _width;
            int height = _height;
            _sourceRectangle = new Rectangle[TotalSpriteFrames];
            for (int i = 0; i < TotalSpriteFrames; i++)
            {
                x = i * width;
                _sourceRectangle[i] = new Rectangle(x, 0, width, height);
            }
        }

        private void SetupTeamRectangles()
        {
            int x_source;
            int x_destination;
            int width = 11;
            int height = 12;
            int index;
            switch (_team)
            {
                case TeamName.Friends: index = 1; break;
                case TeamName.Mobsters: index = 0; break;
                case TeamName.Wilds: index = 2; break;
                default: index = 2; break;
            }
            x_source = index * width;
            _teamSourceRectangle = new Rectangle(x_source, 0, width, height);
            _teamXOffset = _width - width;
            x_destination = _x + _teamXOffset;
            _teamDestinationRectangle = new Rectangle(x_destination, _y, width, height);
        }

        private void SetupGaugeRectangles()
        {
            _gaugeWhite = new Rectangle(0, 0, 1, 1);
            _gaugeBlack = new Rectangle(1, 0, 1, 1);
            _gaugeHPColor = new Rectangle(2, 0, 1, 1);
            _gaugeMPColor = new Rectangle(3, 0, 1, 1);
            int maxHPHeight = (int)Math.Round((float)MaxHP / 2.0f);
            int maxMPHeight = (int)Math.Round((float)MaxMP / 2.0f);
            int hpHeight = (int)Math.Round((float)HP / 2.0f);
            int mpHeight = (int)Math.Round((float)MP / 2.0f);
            int x0 = _x;
            int x1 = x0 + 1;
            int x2 = x0 + 2;
            int x3 = x0 + 3;
            int x4 = x0 + 4;
            int y0 = _y + _gaugeYOffset;
            int y1 = y0 + 1;
            int y2 = y0 + 2;
            _gaugeWhiteHPWidthRectangle = new Rectangle(x0, y1, 5, maxHPHeight + 2);
            _gaugeWhiteHPHeightRectangle = new Rectangle(x1, y0, 3, maxHPHeight + 4);
            _gaugeWhiteMPWidthRectangle = new Rectangle(x2, y1, 5, maxMPHeight + 2);
            _gaugeWhiteMPHeightRectangle = new Rectangle(x3, y0, 3, maxMPHeight + 4);
            _gaugeBlackHPRectangle = new Rectangle(x1, y1, 3, maxHPHeight + 2);
            _gaugeBlackMPRectangle = new Rectangle(x3, y1, 3, maxMPHeight + 2);
            _gaugeHPRectangle = new Rectangle(x2, y2, 1, hpHeight);
            _gaugeMPRectangle = new Rectangle(x4, y2, 1, mpHeight);
        }
        /// <summary>
        /// Updates the gauge destination rectangles based on the current character position given by x and y.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void UpdateGaugeXY(int x, int y)
        {
            int x0 = x;
            int x1 = x0 + 1;
            int x2 = x0 + 2;
            int x3 = x0 + 3;
            int x4 = x0 + 4;
            int y0 = y + _gaugeYOffset;
            int y1 = y0 + 1;
            int y2 = y0 + 2;
            _gaugeWhiteHPWidthRectangle.X = x0;
            _gaugeWhiteHPWidthRectangle.Y = y1;
            _gaugeWhiteHPHeightRectangle.X = x1;
            _gaugeWhiteHPHeightRectangle.Y = y0;
            _gaugeWhiteMPWidthRectangle.X = x2;
            _gaugeWhiteMPWidthRectangle.Y = y1;
            _gaugeWhiteMPHeightRectangle.X = x3;
            _gaugeWhiteMPHeightRectangle.Y = y0;
            _gaugeBlackHPRectangle.X = x1;
            _gaugeBlackHPRectangle.Y = y1;
            _gaugeBlackMPRectangle.X = x3;
            _gaugeBlackMPRectangle.Y = y1;
            _gaugeHPRectangle.X = x2;
            _gaugeHPRectangle.Y = y2;
            _gaugeMPRectangle.X = x4;
            _gaugeMPRectangle.Y = y2;
        }

        protected void SetExploreAgent()
        {
            if (_characterName == CharacterName.KungFu)
            {
                ExploreAgent = _inputManager.GetAgent(AgentName.Player1);
            }
            else
            {
                ExploreAgent = _inputManager.GetAgent(AgentName.CPU);
            }
        }

        protected virtual void SetTeam()
        {
            _team = TeamName.Friends;
        }

        private void SwitchMapTile()
        {
            _moveStepCount = 0;
            if (_mapTile.FriendOnTile != null)
            {
                _mapTile.FriendOnTile = null;
            }
            else
            {
                _mapTile.CharacterOnTile = null;
            }
            _mapTile = _newTile;
            if (_mapTile.CharacterOnTile != null)
            {
                _mapTile.FriendOnTile = this;
            }
            else
            {
                _mapTile.CharacterOnTile = this;
            }
            _needsTileSwitch = false;
            SwitchTileFast = false;
            SwitchTileSlow = false;
        }

        private void ResetSpriteFrameCounters()
        {
            _frameList = _frameOrder.GetFrameList(CurrentAnimation, FacingDirection);
            _spriteFrameLength = _frameList.Count;
            if (_spriteFrameStep >= _spriteFrameLength)
            {
                _spriteFrameStep = 0;
            }
            _spriteFrame = _frameList[_spriteFrameStep];
        }

        public void UpdateCharacter()
        {
            UpdateMapTile();
            UpdateSpritePosition();
            UpdateStandAnimation();
            _destinationRectangle.X = _x;
            _destinationRectangle.Y = _y;

            _spriteStepCount++;
            if (IsMoving)
            {
                _spriteStepCount += _movingStepSpeedup;
            }
            if (_spriteStepCount > SpriteSteps)
            {
                _spriteStepCount = 0;
                _spriteFrameStep++;
                if (_spriteFrameStep >= _spriteFrameLength)
                {
                    _spriteFrameStep = 0;
                }
                _spriteFrame = _frameList[_spriteFrameStep];
            }
        }

        private void UpdateSpritePosition()
        {
            if (IsMoving)
            {
                if (_x > _newX)
                {
                    _x -= _stepSize;
                    if (_x < _newX)
                    {
                        _x = _newX;
                    }
                }
                else if (_x < _newX)
                {
                    _x += _stepSize;
                    if (_x > _newX)
                    {
                        _x = _newX;
                    }
                }
                if (_y > _newY)
                {
                    _y -= _stepSize;
                    if (_y < _newY)
                    {
                        _y = _newY;
                    }
                }
                else if (_y < _newY)
                {
                    _y += _stepSize;
                    if (_y > _newY)
                    {
                        _y = _newY;
                    }
                }

                if ((_x == _newX) && (_y == _newY))
                {
                    IsMoving = false;
                }
            }
        }

        private void UpdateMapTile()
        {
            if (_needsTileSwitch)
            {
                _moveStepCount++;
                if (SwitchTileFast)
                {
                    SwitchMapTile();
                }
                else if (SwitchTileSlow)
                {
                    // Use ">=" instead of ">" to make sure MapTile is switched before unit _isMoving gets reset
                    if (_moveStepCount >= _moveSteps)
                    {
                        SwitchMapTile();
                    }
                }
                else if (_moveStepCount > _tileSwitchSteps)
                {
                    SwitchMapTile();
                }
            }
        }

        private void UpdateStandAnimation()
        {
            if (_needsStandAnimation)
            {
                _animationChangeCount++;
                if (_animationChangeCount > _animationChangeDelay)
                {
                    _needsStandAnimation = false;
                    CurrentAnimation = AnimationSet.stand;
                }
            }
        }
        /// <summary>
        /// Produces a new value for the CurrentSpeed based on the Speed stat and 2 random integers limited by speed.
        /// This function is intended to be called at the start of each battle turn in the setup phase.
        /// </summary>
        protected void UpdateSpeed()
        {
            CurrentSpeed = Speed + NumberGenerator.Next(Speed) + NumberGenerator.Next(Speed);
        }

        /// <summary>
        /// Draws the character sprite
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch to be used for drawing</param>
        /// <param name="viewManager">The ViewManager used for adjusting view</param>
        public void DrawCharacter(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            if (IsOnScreen(viewManager))
            {
                // Draw Character Sprite
                Rectangle destinationRectangle = new Rectangle();
                destinationRectangle.X = _destinationRectangle.X - viewManager.Left;
                destinationRectangle.Y = _destinationRectangle.Y - viewManager.Top;
                destinationRectangle.Width = _destinationRectangle.Width;
                destinationRectangle.Height = _destinationRectangle.Height;
                spriteBatch.Draw(
                    _sprite,
                    destinationRectangle,
                    _sourceRectangle[_spriteFrame],
                    Color.White
                    );

                // Draw Team Icon
                Rectangle teamDestinationRectangle = new Rectangle();
                teamDestinationRectangle.X = destinationRectangle.X + _teamXOffset;
                teamDestinationRectangle.Y = destinationRectangle.Y;
                teamDestinationRectangle.Width = _teamDestinationRectangle.Width;
                teamDestinationRectangle.Height = _teamDestinationRectangle.Height;
                spriteBatch.Draw(
                    _teamTexture,
                    teamDestinationRectangle,
                    _teamSourceRectangle,
                    Color.White
                    );

                DrawGauge(spriteBatch, destinationRectangle.X, destinationRectangle.Y);
            }
        }

        private void DrawGauge(SpriteBatch spriteBatch, int xCharacter, int yCharacter)
        {
            UpdateGaugeXY(xCharacter, yCharacter);
            spriteBatch.Draw(_gaugeTexture, _gaugeWhiteHPWidthRectangle, _gaugeWhite, Color.White);
            spriteBatch.Draw(_gaugeTexture, _gaugeWhiteHPHeightRectangle, _gaugeWhite, Color.White);
            spriteBatch.Draw(_gaugeTexture, _gaugeWhiteMPWidthRectangle, _gaugeWhite, Color.White);
            spriteBatch.Draw(_gaugeTexture, _gaugeWhiteMPHeightRectangle, _gaugeWhite, Color.White);
            spriteBatch.Draw(_gaugeTexture, _gaugeBlackHPRectangle, _gaugeBlack, Color.White);
            spriteBatch.Draw(_gaugeTexture, _gaugeBlackMPRectangle, _gaugeBlack, Color.White);
            spriteBatch.Draw(_gaugeTexture, _gaugeHPRectangle, _gaugeHPColor, Color.White);
            spriteBatch.Draw(_gaugeTexture, _gaugeMPRectangle, _gaugeMPColor, Color.White);
        }
        
        /// <summary>
        /// Returns whether the character is on the screen with position given by viewManager
        /// </summary>
        /// <param name="viewManager">ViewManager to use for checking</param>
        /// <returns></returns>
        public bool IsOnScreen(ViewManager viewManager)
        {
            int viewLeft = viewManager.Left;
            int viewTop = viewManager.Top;
            int viewRight = viewManager.Right;
            int viewBottom = viewManager.Bottom;
            bool onScreen = true;

            if ((_x + _width) < viewLeft) { onScreen = false; }
            else if (_x > viewRight) { onScreen = false; }
            if ((_y + _height) < viewTop) { onScreen = false; }
            else if (_y > viewBottom) { onScreen = false; }

            return onScreen;
        }
    }

    public class Enemy : Character
    {
        public Enemy(SpriteTextureManager spriteTextureManager, MapTile mapTile, InputManager inputManager, CharacterName characterName) :
            base(spriteTextureManager, mapTile, inputManager, characterName)
        {
            _frameOrder = new EnemyFrameOrder();
        }

        protected override int TotalSpriteFrames => 2;

        protected override void SetTeam()
        {
            _team = TeamName.Wilds;
        }
    }

    public class Wahsk: Enemy
    {
        public Wahsk(SpriteTextureManager spriteTextureManager, MapTile mapTile, InputManager inputManager, CharacterName characterName) :
            base(spriteTextureManager, mapTile, inputManager, characterName)
        {
            Speed = 8;
            UpdateSpeed();
        }

        public override float MoveRange => 7.0f;
        public override string ScreenName => "Wahsk";
        public override bool CanFly => true;
        protected override int SpriteSteps => 20; // game steps between sprite frames
    }

    /// <summary>
    /// The FrameOrder class is used to get the array of sprite frame indices based on the current
    /// animation set and the movement direction.
    /// </summary>
    public class FrameOrder
    {
        protected Dictionary<AnimationSet, Dictionary<MoveDirection, List<int>>> _masterFrameDictionary =
            new Dictionary<AnimationSet, Dictionary<MoveDirection, List<int>>>();
        protected Dictionary<MoveDirection, List<int>> _standDictionary = new Dictionary<MoveDirection, List<int>>();
        protected Dictionary<MoveDirection, List<int>> _walkDictionary = new Dictionary<MoveDirection, List<int>>();

        public FrameOrder()
        {
            _standDictionary[MoveDirection.Down] = new List<int> { 0, 1 };
            _standDictionary[MoveDirection.Right] = new List<int> { 2, 3 };
            _standDictionary[MoveDirection.Up] = new List<int> { 4, 5 };
            _standDictionary[MoveDirection.Left] = new List<int> { 6, 7 };

            _walkDictionary[MoveDirection.Down] = new List<int> { 8, 9 };
            _walkDictionary[MoveDirection.Right] = new List<int> { 10, 11, 12, 11 };
            _walkDictionary[MoveDirection.Up] = new List<int> { 13, 14 };
            _walkDictionary[MoveDirection.Left] = new List<int> { 15, 16, 17, 16 };

            _masterFrameDictionary[AnimationSet.stand] = _standDictionary;
            _masterFrameDictionary[AnimationSet.walk] = _walkDictionary;
        }

        public List<int> GetFrameList(AnimationSet animation, MoveDirection direction)
        {
            return (_masterFrameDictionary[animation])[direction];
        }
    }

    public class EnemyFrameOrder : FrameOrder
    {
        public EnemyFrameOrder()
        {
            _standDictionary[MoveDirection.Down] = new List<int> { 0, 1 };
            _standDictionary[MoveDirection.Right] = new List<int> { 0, 1 };
            _standDictionary[MoveDirection.Up] = new List<int> { 0, 1 };
            _standDictionary[MoveDirection.Left] = new List<int> { 0, 1 };

            _walkDictionary[MoveDirection.Down] = new List<int> { 0, 1 };
            _walkDictionary[MoveDirection.Right] = new List<int> { 0, 1 };
            _walkDictionary[MoveDirection.Up] = new List<int> { 0, 1 };
            _walkDictionary[MoveDirection.Left] = new List<int> { 0, 1 };

            _masterFrameDictionary[AnimationSet.stand] = _standDictionary;
            _masterFrameDictionary[AnimationSet.walk] = _walkDictionary;
        }
    }

    public enum AnimationSet
    {
        stand,
        walk
    }

    public struct Stats
    {
        public Stats()
        {
            MaxHP = 11;
            MaxMP = 5;
            Speed = 6;
            Attack = 7;
            Defense = 4;
        }

        public Stats(int maxHP, int maxMP, int speed, int attack, int defense)
        {
            MaxHP = maxHP;
            MaxMP = maxMP;
            Speed = speed;
            Attack = attack;
            Defense = defense;
        }

        public Stats(Stats stats)
        {
            MaxHP = stats.MaxHP;
            MaxMP = stats.MaxMP;
            Speed = stats.Speed;
            Attack = stats.Attack;
            Defense = stats.Defense;
        }
        public int MaxHP { get; set; }
        public int MaxMP { get; set; }
        public int Speed { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
    }

    public enum CharacterName
    {
        None,
        KungFu,
        Temp2,
        Temp3,
        Temp4,
        Wahsk
    }

    public enum TeamName
    {
        None,
        Friends,
        Mobsters,
        Wilds
    }
}
