﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt
{
    public class MenuOption
    {
        private Button _button;
        private string _text;
        private SpriteFont _font;
        private int _xText;
        private int _xGap = 5; // space between lit button and text in pixels
        private int _yButtonOffset = 4; // space to align button with text vertically
        private int _yText;
        private int _width; // full width of MenuOption including button and text
        private Vector2 _textGlowDown;
        private Vector2 _textPosition;
        private Vector2 _textShadowRight;
        private Color _glowColor = new Color(6, 112, 50); // greenish
        public MenuOption(Texture2D buttonTexture, SpriteFont font, string text, int x, int y) :
            this(buttonTexture, font, text, x, y, false)
        {
        }
        public MenuOption(Texture2D buttonTexture, SpriteFont font, string text, int x, int y, bool isActive)
        {
            _button = new Button(buttonTexture, x, y + _yButtonOffset);
            _button.IsActive = isActive;
            _font = font;
            _text = text;
            _xText = x + _button.Width + _xGap;
            _yText = y;
            _width = _button.Width + _xGap + (int)font.MeasureString(text).X;
            _textPosition = new Vector2(_xText, _yText);
            _textGlowDown = new Vector2(_xText, _yText + 1);
            _textShadowRight = new Vector2(_xText + 1, _yText);
        }
        public int Width => _width;
        public bool IsActive => _button.IsActive;
        public bool IsLit
        {
            get => _button.IsLit;
            set => _button.IsLit = value;
        }
        public void Activate()
        {
            _button.IsActive = true;
        }
        public void Deactivate()
        {
            _button.IsActive = false;
        }
        public void Update()
        {
            _button.Update();
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (_button.IsLit)
            {
                _button.Draw(spriteBatch);
                spriteBatch.DrawString(_font, _text, _textGlowDown, _glowColor);
                spriteBatch.DrawString(_font, _text, _textShadowRight, _glowColor);
                spriteBatch.DrawString(_font, _text, _textPosition, Color.White);
            }
            else
            {
                _button.Draw(spriteBatch);
                spriteBatch.DrawString(_font, _text, _textGlowDown, Color.Black);
                spriteBatch.DrawString(_font, _text, _textShadowRight, Color.Black);
                spriteBatch.DrawString(_font, _text, _textPosition, Color.White);
            }
        }
    }
}
