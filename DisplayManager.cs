﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt
{
    public class DisplayManager
    {
        private Game1 _game1;
        private GraphicsDeviceManager _graphics;
        private int _fullScreenHeight;
        private int _fullScreenWidth;
        private int _gameWindowedRawHeight; // gameplay area height before scaling to window
        private int _gameFullscreenRawHeight; //gameplay area height before scaling to fullscreen
        private int _gameScale;
        private int _gameWindowedScale;
        private int _gameFullscreenScale;
        private int _guiScale;
        private int _guiWindowedScale;
        private int _guiFullscreenScale;
        private int _gameFullscreenWidth; // gameplay area width scaled to fullscreen
        private int _gameFullscreenHeight; // gameplay area height scaled to fullscreen
        private int _guiFullscreenWidth;
        private int _guiFullscreenHeight;
        private int _windowWidth;
        private int _windowHeight;
        private int _guiWindowedOffsetX;
        private int _guiWindowedOffsetY;
        private int _gameWindowedOffsetX;
        private int _gameWindowedOffsetY;
        private int _guiFullscreenOffsetX;
        private int _guiFullscreenOffsetY;
        private int _gameFullscreenOffsetX;
        private int _gameFullscreenOffsetY;
        private Rectangle _gameplayWindowSourceRectangle = new Rectangle();
        private Rectangle _gameplayWindowDestinationRectangle = new Rectangle();
        private Rectangle _guiWindowSourceRectangle = new Rectangle();
        private Rectangle _guiWindowDestinationRectangle = new Rectangle();
        private Rectangle _gameplayFullscreenSourceRectangle = new Rectangle();
        private Rectangle _gameplayFullscreenDestinationRectangle = new Rectangle();
        private Rectangle _guiFullscreenSourceRectangle = new Rectangle();
        private Rectangle _guiFullscreenDestinationRectangle = new Rectangle();
        private Rectangle _gameSourceRectangle;
        private Rectangle _guiSourceRectangle;
        private Rectangle _gameDestinationRectangle;
        private Rectangle _guiDestinationRectangle;
        private RenderTarget2D _gameplayWindowRenderTarget;
        private RenderTarget2D _gameplayFullscreenRenderTarget;
        private RenderTarget2D _guiWindowRenderTarget;
        private RenderTarget2D _guiFullscreenRenderTarget;
        private RenderTarget2D _gameRenderTarget;
        private RenderTarget2D _guiRenderTarget;

        public DisplayManager(Game1 game1, GraphicsDeviceManager graphics)
        {
            _game1 = game1;
            _graphics = graphics;
        }

        public int GameScale
        {
            get => _gameScale;
        }

        public int GuiScale
        {
            get => _guiScale;
        }

        public Rectangle GameSourceRectangle
        {
            get => _gameSourceRectangle;
        }

        public Rectangle GuiSourceRectangle
        {
            get => _guiSourceRectangle;
        }

        public Rectangle GameDestinationRectangle
        {
            get => _gameDestinationRectangle;
        }

        public Rectangle GuiDestinationRectangle
        {
            get => _guiDestinationRectangle;
        }

        public RenderTarget2D GameRenderTarget
        {
            get => _gameRenderTarget;
        }

        public RenderTarget2D GuiRenderTarget
        {
            get => _guiRenderTarget;
        }

        /// <summary>
        /// Adjusts the backbuffer size when changing between windowed or fullscreen mode.
        /// </summary>
        public void AdjustBackBufferSize()
        {
            if (_graphics.IsFullScreen)
            {
                _graphics.PreferredBackBufferWidth = _fullScreenWidth;
                _graphics.PreferredBackBufferHeight = _fullScreenHeight;
            }
            else
            {
                _graphics.PreferredBackBufferWidth = _windowWidth;
                _graphics.PreferredBackBufferHeight = _windowHeight;
            }
            _graphics.ApplyChanges();
        }

        public void InitializeDisplayManager()
        {
            SetupScreenScaling();
            SetupRenderTargets();
            _gameScale = _gameWindowedScale;
            _guiScale = _guiWindowedScale;
            _gameSourceRectangle = _gameplayWindowSourceRectangle;
            _gameDestinationRectangle = _gameplayWindowDestinationRectangle;
            _guiSourceRectangle = _guiWindowSourceRectangle;
            _guiDestinationRectangle = _guiWindowDestinationRectangle;
            AdjustBackBufferSize();
        }

        /// <summary>
        /// Sets the dimensions for drawing the gameplay area and GUI to the screen.
        /// </summary>
        public void SetupScreenScaling()
        {
            int titleHeight = 30; // height of Windows title bar
            int taskbarHeight = 40; // height of Windows taskbar
            _fullScreenHeight = _game1.GraphicsDevice.DisplayMode.Height;
            int windowedScreenHeight = _game1.GraphicsDevice.DisplayMode.Height - titleHeight - taskbarHeight;
            _fullScreenWidth = _game1.GraphicsDevice.DisplayMode.Width;
            int windowedScreenWidth = _fullScreenWidth - 2;
            int windowedScaleFactor;
            int windowedUncutScaleFactor; // factor for scaling game to maximum height in tiles
            int guiWindowedMaxWidthScale;
            int guiWindowedMaxHeightScale;
            int fullscreenScaleFactor;
            int fullscreenUncutScaleFactor; // factor for scaling game to maximum height in tiles
            int guiFullscreenMaxWidthScale;
            int guiFullscreenMaxHeightScale;
            int guiWindowedScaledWidth;
            int guiWindowedScaledHeight;
            int gameWindowedScaledWidth;
            int gameWindowedScaledHeight;

            fullscreenScaleFactor = _fullScreenHeight / _game1._gameHeightMin;
            fullscreenUncutScaleFactor = _fullScreenHeight / _game1._gameHeightMax;
            guiFullscreenMaxWidthScale = _fullScreenWidth / _game1._guiWidth;
            guiFullscreenMaxHeightScale = _fullScreenHeight / _game1._guiHeight;

            windowedScaleFactor = windowedScreenHeight / _game1._gameHeightMin;
            windowedUncutScaleFactor = windowedScreenHeight / _game1._gameHeightMax;
            guiWindowedMaxWidthScale = windowedScreenWidth / _game1._guiWidth;
            guiWindowedMaxHeightScale = windowedScreenHeight / _game1._guiHeight;

            _gameFullscreenScale = Math.Max(fullscreenScaleFactor, fullscreenUncutScaleFactor);
            _gameWindowedScale = Math.Max(windowedScaleFactor, windowedUncutScaleFactor);
            _gameFullscreenWidth = _gameFullscreenScale * _game1._gameWidth;
            gameWindowedScaledWidth = _gameWindowedScale * _game1._gameWidth;

            // if game can be scaled up more with cutting, use cut game height
            if (fullscreenUncutScaleFactor < fullscreenScaleFactor)
            {
                _gameFullscreenHeight = _fullScreenHeight;
                _gameFullscreenRawHeight = _gameFullscreenHeight / _gameFullscreenScale;
            }
            else
            {
                _gameFullscreenHeight = _gameFullscreenScale * _game1._gameHeightMax;
                _gameFullscreenRawHeight = _game1._gameHeightMax;
            }
            // if game can be scaled up more with cutting, use cut game height
            if (windowedUncutScaleFactor < windowedScaleFactor)
            {
                gameWindowedScaledHeight = windowedScreenHeight;
                _gameWindowedRawHeight = gameWindowedScaledHeight / _gameWindowedScale;
            }
            else
            {
                gameWindowedScaledHeight = _gameWindowedScale * _game1._gameHeightMax;
                _gameWindowedRawHeight = _game1._gameHeightMax;
            }

            _guiFullscreenScale = Math.Min(guiFullscreenMaxWidthScale, guiFullscreenMaxHeightScale);
            _guiWindowedScale = Math.Min(guiWindowedMaxWidthScale, guiWindowedMaxHeightScale);
            _guiFullscreenWidth = _guiFullscreenScale * _game1._guiWidth;
            _guiFullscreenHeight = _guiFullscreenScale * _game1._guiHeight;
            guiWindowedScaledWidth = _guiWindowedScale * _game1._guiWidth;
            guiWindowedScaledHeight = _guiWindowedScale * _game1._guiHeight;

            _windowWidth = Math.Max(guiWindowedScaledWidth, gameWindowedScaledWidth);
            _windowHeight = Math.Max(guiWindowedScaledHeight, gameWindowedScaledHeight);

            _gameFullscreenOffsetX = (_fullScreenWidth - _gameFullscreenWidth) / (2 * _gameFullscreenScale);
            _gameFullscreenOffsetY = (_fullScreenHeight - _gameFullscreenHeight) / (2 * _gameFullscreenScale);
            _guiFullscreenOffsetX = (_fullScreenWidth - _guiFullscreenWidth) / (2 * _guiFullscreenScale);
            _guiFullscreenOffsetY = (_fullScreenHeight - _guiFullscreenHeight) / (2 * _guiFullscreenScale);

            _gameWindowedOffsetX = (_windowWidth - gameWindowedScaledWidth) / (2 * _gameWindowedScale);
            _gameWindowedOffsetY = (_windowHeight - gameWindowedScaledHeight) / (2 * _gameWindowedScale);
            _guiWindowedOffsetX = (_windowWidth - guiWindowedScaledWidth) / (2 * _guiWindowedScale);
            _guiWindowedOffsetY = (_windowHeight - guiWindowedScaledHeight) / (2 * _guiWindowedScale);

            _gameplayWindowSourceRectangle.X = 0;
            _gameplayWindowSourceRectangle.Y = 0;
            _gameplayWindowSourceRectangle.Width = _game1._gameWidth;
            _gameplayWindowSourceRectangle.Height = _gameWindowedRawHeight;

            _gameplayWindowDestinationRectangle.X = _gameWindowedOffsetX;
            _gameplayWindowDestinationRectangle.Y = _gameWindowedOffsetY;
            _gameplayWindowDestinationRectangle.Width = _gameplayWindowSourceRectangle.Width;
            _gameplayWindowDestinationRectangle.Height = _gameplayWindowSourceRectangle.Height;

            _gameplayFullscreenSourceRectangle.X = 0;
            _gameplayFullscreenSourceRectangle.Y = 0;
            _gameplayFullscreenSourceRectangle.Width = _game1._gameWidth;
            _gameplayFullscreenSourceRectangle.Height = _gameFullscreenRawHeight;

            _gameplayFullscreenDestinationRectangle.X = _gameFullscreenOffsetX;
            _gameplayFullscreenDestinationRectangle.Y = _gameFullscreenOffsetY;
            _gameplayFullscreenDestinationRectangle.Width = _gameplayFullscreenSourceRectangle.Width;
            _gameplayFullscreenDestinationRectangle.Height = _gameplayFullscreenSourceRectangle.Height;

            _guiWindowSourceRectangle.X = 0;
            _guiWindowSourceRectangle.Y = 0;
            _guiWindowSourceRectangle.Width = _game1._guiWidth;
            _guiWindowSourceRectangle.Height = _game1._guiHeight;

            _guiWindowDestinationRectangle.X = _guiWindowedOffsetX;
            _guiWindowDestinationRectangle.Y = _guiWindowedOffsetY;
            _guiWindowDestinationRectangle.Width = _guiWindowSourceRectangle.Width;
            _guiWindowDestinationRectangle.Height = _guiWindowSourceRectangle.Height;

            _guiFullscreenSourceRectangle.X = 0;
            _guiFullscreenSourceRectangle.Y = 0;
            _guiFullscreenSourceRectangle.Width = _game1._guiWidth;
            _guiFullscreenSourceRectangle.Height = _game1._guiHeight;

            _guiFullscreenDestinationRectangle.X = _guiFullscreenOffsetX;
            _guiFullscreenDestinationRectangle.Y = _guiFullscreenOffsetY;
            _guiFullscreenDestinationRectangle.Width = _guiFullscreenSourceRectangle.Width;
            _guiFullscreenDestinationRectangle.Height = _guiFullscreenSourceRectangle.Height;
        }

        protected void SetupRenderTargets()
        {
            _gameplayWindowRenderTarget = new RenderTarget2D(_game1.GraphicsDevice, _game1._gameWidth, _gameWindowedRawHeight);
            _gameplayFullscreenRenderTarget = new RenderTarget2D(_game1.GraphicsDevice, _game1._gameWidth, _gameFullscreenRawHeight);
            _guiWindowRenderTarget = new RenderTarget2D(_game1.GraphicsDevice, _game1._guiWidth, _game1._guiHeight);
            _guiFullscreenRenderTarget = new RenderTarget2D(_game1.GraphicsDevice, _game1._guiWidth, _game1._guiHeight);
            _gameRenderTarget = _gameplayWindowRenderTarget;
            _guiRenderTarget = _guiWindowRenderTarget;
        }

        public void ToggleFullScreen()
        {
            if (_graphics.IsFullScreen)
            {
                _graphics.IsFullScreen = false;
                _gameScale = _gameWindowedScale;
                _guiScale = _guiWindowedScale;
                _gameRenderTarget = _gameplayWindowRenderTarget;
                _guiRenderTarget = _guiWindowRenderTarget;
                _gameSourceRectangle = _gameplayWindowSourceRectangle;
                _gameDestinationRectangle = _gameplayWindowDestinationRectangle;
                _guiSourceRectangle = _guiWindowSourceRectangle;
                _guiDestinationRectangle = _guiWindowDestinationRectangle;
            }
            else
            {
                _graphics.IsFullScreen = true;
                _gameScale = _gameFullscreenScale;
                _guiScale = _guiFullscreenScale;
                _gameRenderTarget = _gameplayFullscreenRenderTarget;
                _guiRenderTarget = _guiFullscreenRenderTarget;
                _gameSourceRectangle = _gameplayFullscreenSourceRectangle;
                _gameDestinationRectangle = _gameplayFullscreenDestinationRectangle;
                _guiSourceRectangle = _guiFullscreenSourceRectangle;
                _guiDestinationRectangle = _guiFullscreenDestinationRectangle;
            }
            _graphics.ApplyChanges();
            AdjustBackBufferSize();
        }
    }
}
