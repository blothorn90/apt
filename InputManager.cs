﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace apt
{
    public class InputManager
    {
        private List<InputDevice> _inputDevices = new List<InputDevice>()
        {
            new KeyboardDevice(),
            new GamepadDevice(PlayerIndex.One, DeviceName.Gamepad1),
            new GamepadDevice(PlayerIndex.Two, DeviceName.Gamepad2)
        };
        private List<Agent> _allAgents = new List<Agent>() {
            new Agent(AgentName.CPU),
            new Agent(AgentName.Player1),
            new Agent(AgentName.Player2)
        };
        private Dictionary<AgentName, Agent> _agentDictionary = new Dictionary<AgentName, Agent>();
        private Dictionary<DeviceName, InputDevice> _deviceDictionary = new Dictionary<DeviceName, InputDevice>();
        private int _availableDevices;


        public InputManager()
        {
            CheckConnections();
            _agentDictionary[AgentName.CPU] = _allAgents[0];
            _agentDictionary[AgentName.Player1] = _allAgents[1];
            _agentDictionary[AgentName.Player2] = _allAgents[2];

            _deviceDictionary[DeviceName.Keyboard] = _inputDevices[0];
            _deviceDictionary[DeviceName.Gamepad1] = _inputDevices[1];
            _deviceDictionary[DeviceName.Gamepad2] = _inputDevices[2];

            SetAgentDefaultDevices();
        }

        public int NumberOfInputs => _availableDevices;
        public bool IsPausePressed
        {
            get
            {
                foreach (InputDevice device in _inputDevices)
                {
                    if (device.IsPausePressed) {  return true; }
                }
                return false;
            }
        }

        public void CheckConnections()
        {
            int deviceCount = 0;
            foreach (InputDevice device in _inputDevices)
            {
                if (device.IsAvailable)
                {
                    deviceCount++;
                }
            }
            if (deviceCount < 2)
            {
                _allAgents[2].IsActive = false;
            }
            _availableDevices = deviceCount;
        }

        private void SetAgentDefaultDevices()
        {
            switch (_availableDevices)
            {
                case 1:
                    _agentDictionary[AgentName.Player1].DeviceUsed = _deviceDictionary[DeviceName.Keyboard];
                    break;
                case 2:
                    _agentDictionary[AgentName.Player1].DeviceUsed = _deviceDictionary[DeviceName.Keyboard];
                    _agentDictionary[AgentName.Player2].DeviceUsed = _deviceDictionary[DeviceName.Gamepad1];
                    break;
                case 3:
                    _agentDictionary[AgentName.Player1].DeviceUsed = _deviceDictionary[DeviceName.Gamepad1];
                    _agentDictionary[AgentName.Player2].DeviceUsed = _deviceDictionary[DeviceName.Gamepad2];
                    break;
            }
        }

        public void UpdateInputs()
        {
            foreach (InputDevice device in _inputDevices)
            {
                device.UpdateDevice();
            }
        }

        public Agent GetAgent(AgentName agentName)
        {
            return _agentDictionary[agentName];
        }
    }

    public abstract class InputDevice
    {
        private DeviceName _deviceName;

        public DeviceName DeviceName
        {
            get
            {
                return _deviceName;
            }
        }

        public virtual bool IsAvailable => true;

        public InputDevice(DeviceName deviceName)
        {
            _deviceName = deviceName;
        }

        public abstract bool IsLeftDown { get; }
        public abstract bool IsLeftPressed { get; }
        public abstract bool IsRightDown { get; }
        public abstract bool IsRightPressed { get; }
        public abstract bool IsUpDown { get; }
        public abstract bool IsUpPressed { get; }
        public abstract bool IsDownDown { get; }
        public abstract bool IsDownPressed { get; }
        public abstract bool IsActionPressed { get; }
        public abstract bool IsCancelPressed { get; }
        public abstract bool IsAttackPressed { get; }
        public abstract bool IsMagicPressed { get; }
        public abstract bool IsItemPressed { get; }
        public abstract bool IsMenuPressed { get; }
        public abstract bool IsPausePressed { get; }

        public abstract void UpdateDevice();
    }

    public class KeyboardDevice : InputDevice
    {
        private readonly Keys _actionKey = Keys.Enter;
        private readonly Keys _cancelKey = Keys.Escape;
        private readonly Keys _attackKey = Keys.X;
        private readonly Keys _magicKey = Keys.Y;
        private readonly Keys _itemKey = Keys.I;
        private readonly Keys _menuKey = Keys.M;
        private readonly Keys _pauseKey = Keys.P;
        private KeyboardState _previousKeyboardState;
        private KeyboardState _currentKeyboardState;

        public override bool IsLeftDown => _currentKeyboardState.IsKeyDown(Keys.Left);
        public override bool IsLeftPressed
        {
            get
            {
                return IsLeftDown && !_previousKeyboardState.IsKeyDown(Keys.Left);
            }
        }
        public override bool IsRightDown => _currentKeyboardState.IsKeyDown(Keys.Right);
        public override bool IsRightPressed
        {
            get
            {
                return IsRightDown && !_previousKeyboardState.IsKeyDown(Keys.Right);
            }
        }
        public override bool IsUpDown => _currentKeyboardState.IsKeyDown(Keys.Up);
        public override bool IsUpPressed
        {
            get
            {
                return IsUpDown && !_previousKeyboardState.IsKeyDown(Keys.Up);
            }
        }
        public override bool IsDownDown => _currentKeyboardState.IsKeyDown(Keys.Down);
        public override bool IsDownPressed
        {
            get
            {
                // expanded function for setting debugging breakpoints based on return value
                if (IsDownDown && !_previousKeyboardState.IsKeyDown(Keys.Down))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public override bool IsActionPressed
        {
            get
            {
                return _currentKeyboardState.IsKeyDown(_actionKey) &&
                    !_previousKeyboardState.IsKeyDown(_actionKey);
            }
        }
        public override bool IsCancelPressed
        {
            get
            {
                return _currentKeyboardState.IsKeyDown(_cancelKey) &&
                    !_previousKeyboardState.IsKeyDown(_cancelKey);
            }
        }
        public override bool IsAttackPressed
        {
            get
            {
                return _currentKeyboardState.IsKeyDown(_attackKey) &&
                    !_previousKeyboardState.IsKeyDown(_attackKey);
            }
        }
        public override bool IsMagicPressed
        {
            get
            {
                return _currentKeyboardState.IsKeyDown(_magicKey) &&
                    !_previousKeyboardState.IsKeyDown(_magicKey);
            }
        }
        public override bool IsItemPressed
        {
            get
            {
                return _currentKeyboardState.IsKeyDown(_itemKey) &&
                    !_previousKeyboardState.IsKeyDown(_itemKey);
            }
        }
        public override bool IsMenuPressed
        {
            get
            {
                return _currentKeyboardState.IsKeyDown(_menuKey) &&
                    !_previousKeyboardState.IsKeyDown(_menuKey);
            }
        }
        public override bool IsPausePressed
        {
            get
            {
                return _currentKeyboardState.IsKeyDown(_pauseKey) &&
                    !_previousKeyboardState.IsKeyDown(_pauseKey);
            }
        }

        public KeyboardDevice(): base(DeviceName.Keyboard)
        {
            _currentKeyboardState = Keyboard.GetState();
            _previousKeyboardState = _currentKeyboardState;
        }

        public override void UpdateDevice()
        {
            _previousKeyboardState = _currentKeyboardState;
            _currentKeyboardState = Keyboard.GetState();
        }
    }

    /// <summary>
    /// Important: When instantiating a GamepadDevice, make sure the PlayerIndex corresponds to the
    /// correct DeviceName. For example, PlayerIndex.One goes with DeviceName.Gamepad1.
    /// </summary>
    public class GamepadDevice : InputDevice
    {
        private GamePadState _previousGamepadState;
        private GamePadState _currentGamepadState;
        private PlayerIndex _playerIndex;
        private float _deadzone = 0.4f;
        private Buttons _actionButton = Buttons.A;
        private Buttons _cancelButton = Buttons.B;
        private Buttons _attackButton = Buttons.X;
        private Buttons _magicButton = Buttons.Y;
        private Buttons _itemButton = Buttons.LeftShoulder;
        private Buttons _menuButton = Buttons.RightShoulder;
        private Buttons _pauseButton = Buttons.Start;

        public override bool IsLeftDown => IsLeftAxisDown() || IsLeftDpadDown();
        public override bool IsLeftPressed => IsLeftDown && !WasLeftDown();
        public override bool IsRightDown => IsRightAxisDown() || IsRightDpadDown();
        public override bool IsRightPressed => IsRightDown && !WasRightDown();
        public override bool IsUpDown => IsUpAxisDown() || IsUpDpadDown();
        public override bool IsUpPressed => IsUpDown && !WasUpDown();
        public override bool IsDownDown => IsDownAxisDown() || IsDownDpadDown();
        public override bool IsDownPressed => IsDownDown && !WasDownDown();
        public override bool IsActionPressed
        {
            get
            {
                return _currentGamepadState.IsButtonDown(_actionButton) &&
                    !_previousGamepadState.IsButtonDown(_actionButton);
            }
        }
        public override bool IsCancelPressed
        {
            get
            {
                return _currentGamepadState.IsButtonDown(_cancelButton) &&
                    !_previousGamepadState.IsButtonDown(_cancelButton);
            }
        }
        public override bool IsAttackPressed
        {
            get
            {
                return _currentGamepadState.IsButtonDown(_attackButton) &&
                    !_previousGamepadState.IsButtonDown(_attackButton);
            }
        }
        public override bool IsMagicPressed
        {
            get
            {
                return _currentGamepadState.IsButtonDown(_magicButton) &&
                    !_previousGamepadState.IsButtonDown(_magicButton);
            }
        }
        public override bool IsItemPressed
        {
            get
            {
                return _currentGamepadState.IsButtonDown(_itemButton) &&
                    !_previousGamepadState.IsButtonDown(_itemButton);
            }
        }
        public override bool IsMenuPressed
        {
            get
            {
                return _currentGamepadState.IsButtonDown(_menuButton) &&
                    !_previousGamepadState.IsButtonDown(_menuButton);
            }
        }
        public override bool IsPausePressed
        {
            get
            {
                return _currentGamepadState.IsButtonDown(_pauseButton) &&
                    !_previousGamepadState.IsButtonDown(_pauseButton);
            }
        }
        public override bool IsAvailable => _currentGamepadState.IsConnected;

        public GamepadDevice(PlayerIndex playerIndex, DeviceName deviceName) : base(deviceName)
        {
            _playerIndex = playerIndex;
            _currentGamepadState = GamePad.GetState(_playerIndex);
            _previousGamepadState = _currentGamepadState;
        }


        // Left
        private bool IsLeftAxisDown()
        {
            return _currentGamepadState.ThumbSticks.Left.X < -_deadzone;
        }
        private bool WasLeftAxisDown()
        {
            return _previousGamepadState.ThumbSticks.Left.X < -_deadzone;
        }
        private bool IsLeftDpadDown()
        {
            return _currentGamepadState.IsButtonDown(Buttons.DPadLeft);
        }
        private bool WasLeftDpadDown()
        {
            return _previousGamepadState.IsButtonDown(Buttons.DPadLeft);
        }
        private bool WasLeftDown()
        {
            return WasLeftAxisDown() || WasLeftDpadDown();
        }

        // Right
        private bool IsRightAxisDown()
        {
            return _currentGamepadState.ThumbSticks.Left.X > _deadzone;
        }
        private bool WasRightAxisDown()
        {
            return _previousGamepadState.ThumbSticks.Left.X > _deadzone;
        }
        private bool IsRightDpadDown()
        {
            return _currentGamepadState.IsButtonDown(Buttons.DPadRight);
        }
        private bool WasRightDpadDown()
        {
            return _previousGamepadState.IsButtonDown(Buttons.DPadRight);
        }
        private bool WasRightDown()
        {
            return WasRightAxisDown() || WasRightDpadDown();
        }

        // Up
        private bool IsUpAxisDown()
        {
            return _currentGamepadState.ThumbSticks.Left.Y > _deadzone;
        }
        private bool WasUpAxisDown()
        {
            return _previousGamepadState.ThumbSticks.Left.Y > _deadzone;
        }
        private bool IsUpDpadDown()
        {
            return _currentGamepadState.IsButtonDown(Buttons.DPadUp);
        }
        private bool WasUpDpadDown()
        {
            return _previousGamepadState.IsButtonDown(Buttons.DPadUp);
        }
        private bool WasUpDown()
        {
            return WasUpAxisDown() || WasUpDpadDown();
        }

        // Down
        private bool IsDownAxisDown()
        {
            return _currentGamepadState.ThumbSticks.Left.Y < -_deadzone;
        }
        private bool WasDownAxisDown()
        {
            return _previousGamepadState.ThumbSticks.Left.Y < -_deadzone;
        }
        private bool IsDownDpadDown()
        {
            return _currentGamepadState.IsButtonDown(Buttons.DPadDown);
        }
        private bool WasDownDpadDown()
        {
            return _previousGamepadState.IsButtonDown(Buttons.DPadDown);
        }
        private bool WasDownDown()
        {
            return WasDownAxisDown() || WasDownDpadDown();
        }

        public override void UpdateDevice()
        {
            _previousGamepadState = _currentGamepadState;
            _currentGamepadState = GamePad.GetState(_playerIndex);
        }
    }

    public class Agent
    {
        private readonly AgentName _agentName;
        private InputDevice _inputDevice;
        private DeviceName _currentDeviceName = DeviceName.None;

        public AgentName AgentName
        {
            get
            {
                return _agentName;
            }
        }

        public bool IsActive { get; set; } = true;

        public bool IsLeftDown
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsLeftDown;
                }
            }
        }
        public bool IsLeftPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsLeftPressed;
                }
            }
        }
        public bool IsRightDown
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsRightDown;
                }
            }
        }
        public bool IsRightPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsRightPressed;
                }
            }
        }
        public bool IsUpDown
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsUpDown;
                }
            }
        }
        public bool IsUpPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsUpPressed;
                }
            }
        }
        public bool IsDownDown
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsDownDown;
                }
            }
        }
        public bool IsDownPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsDownPressed;
                }
            }
        }
        public bool IsActionPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsActionPressed;
                }
            }
        }
        public bool IsCancelPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsCancelPressed;
                }
            }
        }
        public bool IsAttackPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsAttackPressed;
                }
            }
        }
        public bool IsMagicPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsMagicPressed;
                }
            }
        }
        public bool IsItemPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsItemPressed;
                }
            }
        }
        public bool IsMenuPressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsMenuPressed;
                }
            }

        }
        public bool IsPausePressed
        {
            get
            {
                if (_currentDeviceName == DeviceName.None)
                {
                    return false;
                }
                else
                {
                    return _inputDevice.IsPausePressed;
                }
            }
        }

        public Agent(AgentName agentName)
        {
            _agentName = agentName;
        }

        public InputDevice DeviceUsed
        {
            get
            {
                return _inputDevice;
            }
            set
            {
                _inputDevice = value;
                _currentDeviceName = _inputDevice.DeviceName;
            }
        }
    }

    public enum DeviceName
    {
        None,
        Keyboard,
        Gamepad1,
        Gamepad2
    }

    public enum AgentName
    {
        CPU,
        Player1,
        Player2
    }
}
