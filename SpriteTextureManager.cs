﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace apt
{
    public class SpriteTextureManager
    {
        private Dictionary<CharacterName, Texture2D> _spriteTextures = new Dictionary<CharacterName, Texture2D>();
        private Texture2D _gaugeTexture;
        private Texture2D _teamTexture;
        private Texture2D _cursorBGTexture;
        private Texture2D _cursorFGTexture;
        public SpriteTextureManager()
        {
        }
        public Texture2D TeamTexture => _teamTexture;
        public Texture2D GaugeTexture => _gaugeTexture;
        public Texture2D CursorBGTexture => _cursorBGTexture;
        public Texture2D CursorFGTexture => _cursorFGTexture;
        public void LoadSpriteTextures(Game1 game1)
        {
            _spriteTextures[CharacterName.KungFu] = game1.Content.Load<Texture2D>("kungFuChar");
            _spriteTextures[CharacterName.Temp2] = game1.Content.Load<Texture2D>("tempChar2");
            _spriteTextures[CharacterName.Temp3] = game1.Content.Load<Texture2D>("tempChar3");
            _spriteTextures[CharacterName.Temp4] = game1.Content.Load<Texture2D>("tempChar4");
            _spriteTextures[CharacterName.Wahsk] = game1.Content.Load<Texture2D>("wahsk");
            _gaugeTexture = game1.Content.Load<Texture2D>("gauge");
            _teamTexture = game1.Content.Load<Texture2D>("team");
            _cursorBGTexture = game1.Content.Load<Texture2D>("cursorBG");
            _cursorFGTexture = game1.Content.Load<Texture2D>("cursorFG");
        }

        public Texture2D SpriteTexture(CharacterName characterName)
        {
            return _spriteTextures[characterName];
        }
    }
}
