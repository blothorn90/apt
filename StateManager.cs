﻿using System;
using System.Collections.Generic;

namespace apt
{
    public class StateManager
    {
        private Game1 _game1;
        private Stack<GameState> _stateStack = new Stack<GameState>();
        private GameState _currentState;
        private MapReader _mapReader;
        private InputManager _inputManager;
        private ViewManager _viewManager;
        private GuiManager _guiManager;
        private int _numberOfPlayers;
        public StateManager(Game1 game1, InputManager inputManager)
        {
            _game1 = game1;
            _inputManager = inputManager;
            InitializePlayerCount();
            _currentState = new BattleState(inputManager, this);
            _mapReader = null;
        }

        public int NumberOfPlayers
        {
            get
            {
                return _numberOfPlayers;
            }
            set
            {
                _numberOfPlayers = value;
            }
        }
        public MapReader UsingMapReader
        {
            get
            {
                return _mapReader;
            }
            set
            {
                _mapReader = value;
                GameStateType currentStateType = _currentState.StateType;
                if ((currentStateType == GameStateType.Explore) || (currentStateType == GameStateType.Battle))
                {
                    _currentState.UsingMapReader = _mapReader;
                }
            }
        }
        public ViewManager UsingViewManager
        {
            get
            {
                return _viewManager;
            }
            set
            {
                _viewManager = value;
            }
        }
        public GuiManager UsingGuiManager
        {
            get
            {
                return _guiManager;
            }
            set
            {
                _guiManager = value;
            }
        }

        private void InitializePlayerCount()
        {
            switch (_inputManager.NumberOfInputs)
            {
                case 1:
                    _numberOfPlayers = 1;
                    break;
                case 2:
                case 3:
                    _numberOfPlayers = 2;
                    break;
                default:
                    _numberOfPlayers = 1;
                    break;
            }
        }

        public void Update()
        {
            _inputManager.UpdateInputs();
            if (_inputManager.IsPausePressed)
            {
                _game1.GameIsPaused = !_game1.GameIsPaused;
            }
            if (!_game1.GameIsPaused)
            {
                _currentState.Update();
            }
        }

        public void SwitchToPreviousState()
        {
            _currentState = _stateStack.Pop();
            if (_currentState.StateType == GameStateType.Battle)
            {
                UsingViewManager.Tracking2 = null;
            }
        }

        public void SwitchToNewState(GameState newState)
        {
            _stateStack.Push(_currentState);
            _currentState = newState;
            if (_currentState.StateType == GameStateType.Battle)
            {
                UsingViewManager.Tracking2 = null;
            }
        }
    }

    public class GameState
    {
        protected GameStateType _stateType;
        protected InputManager _inputManager;
        protected StateManager _stateManager;
        protected MapReader _mapReader;
        protected MoveManager _moveManager;

        public GameState(InputManager inputManager, StateManager stateManager)
        {
            _inputManager = inputManager;
            _stateManager = stateManager;
        }
        public GameStateType StateType
        {
            get
            {
                return _stateType;
            }
        }

        public MapReader UsingMapReader
        {
            get
            {
                return _mapReader;
            }
            set
            {
                _mapReader = value;
                _moveManager.UsingMapReader = _mapReader;
            }
        }

        protected void MovePlayer(Character character, MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.Right:
                    if (_moveManager.IsMoveRightOK(character))
                    {
                        _moveManager.MoveRight(character);
                    }
                    break;
                case MoveDirection.Up:
                    if (_moveManager.IsMoveUpOK(character))
                    {
                        _moveManager.MoveUp(character);
                    }
                    break;
                case MoveDirection.Left:
                    if (_moveManager.IsMoveLeftOK(character))
                    {
                        _moveManager.MoveLeft(character);
                    }
                    break;
                case MoveDirection.Down:
                    if (_moveManager.IsMoveDownOK(character))
                    {
                        _moveManager.MoveDown(character);
                    }
                    break;
            }
        }

        protected MoveDirection GetInputDirection(AgentName agentName)
        {
            MoveDirection direction = MoveDirection.None;
            Agent playerAgent = _inputManager.GetAgent(agentName);
            if (playerAgent.IsRightDown)
            {
                direction = MoveDirection.Right;
            }
            else if (playerAgent.IsUpDown) { direction = MoveDirection.Up; }
            else if (playerAgent.IsLeftDown) { direction = MoveDirection.Left; }
            else if (playerAgent.IsDownDown) { direction = MoveDirection.Down; }
            return direction;
        }
        protected MoveDirection GetInputDirectionPressed(AgentName agentName)
        {
            MoveDirection direction = MoveDirection.None;
            Agent playerAgent = _inputManager.GetAgent(agentName);
            if (playerAgent.IsRightPressed)
            {
                direction = MoveDirection.Right;
            }
            else if (playerAgent.IsUpPressed) { direction = MoveDirection.Up; }
            else if (playerAgent.IsLeftPressed) { direction = MoveDirection.Left; }
            else if (playerAgent.IsDownPressed) { direction = MoveDirection.Down; }
            return direction;
        }

        public virtual void Update()
        {
            _stateManager.UsingGuiManager.Update();
        }
    }

    public class ExploreState : GameState
    {
        private bool _stateReady = false;
        private Character _player1;
        private Character _player2;

        public ExploreState(InputManager inputManager, StateManager stateManager) : base(inputManager, stateManager)
        {
            _stateType = GameStateType.Explore;
            _moveManager = new ExploreMoveManager(_mapReader);
        }

        protected virtual void PrepareAgents()
        {
            foreach (Character character in _mapReader.CharacterList)
            {
                if (character.ExploreAgent.AgentName == AgentName.Player1)
                {
                    _player1 = character;
                    _stateManager.UsingViewManager.Tracking1 = character;
                }
                else if (character.ExploreAgent.AgentName == AgentName.Player2)
                {
                    _player2 = character;
                    _stateManager.UsingViewManager.Tracking2 = character;
                }
            }
        }

        protected void CheckPlayerInput(AgentName agentName)
        {
            MoveDirection direction;
            Character character;
            Agent playerAgent = _inputManager.GetAgent(agentName);
            if (agentName == AgentName.Player1)
            {
                character = _player1;
            }
            else
            {
                character = _player2;
            }
            if (_stateManager.UsingGuiManager.AcceptingInput)
            {
                direction = GetInputDirectionPressed(agentName);
                _stateManager.UsingGuiManager.MoveSelection(character, direction);
                if (playerAgent.IsCancelPressed)
                {
                    _stateManager.UsingGuiManager.CloseMenu(character);
                }
                else if (playerAgent.IsActionPressed)
                {
                    _stateManager.UsingGuiManager.ExecuteOption(character);
                }
            }
            else
            {
                direction = GetInputDirection(agentName);
                MovePlayer(character, direction);
                if (playerAgent.IsMenuPressed && !_stateManager.UsingGuiManager.WindowActive)
                {
                    _stateManager.UsingGuiManager.OpenMenu(character);
                }
            }
        }

        public override void Update()
        {
            if (_mapReader != null)
            {
                _mapReader.UpdateMap();
                if (!_stateReady)
                {
                    PrepareAgents();
                    _stateReady = true;
                }
                else
                {
                    CheckPlayerInput(AgentName.Player1);
                    if (_stateManager.NumberOfPlayers == 2)
                    {
                        CheckPlayerInput(AgentName.Player2);
                    }
                }
            }
            base.Update();
        }
    }
    public class BattleState : GameState
    {
        private Character _currentCharacter;
        private Cursor _cursor;
        private int _currentCharacterIndex = 0;
        private int _minimumActionCost = 3; // minimum number of action points required for attack, magic, or item use
        private int _attackCost = 3;
        private int _magicCost = 4; // action points consumed when using magic in combat
        private int _itemCost = 4; // action points consumed when using an item in combat
        private BattlePhase _phase = BattlePhase.Setup;
        private ActionStep _actionStep = ActionStep.Moving;
        private bool _moveListsReady = false;
        private bool _selectingTarget = false; // whether character is selecting a target
        private readonly Random RandomGenerator = new Random();
        private List<AllowedMove> _safeMoves = new List<AllowedMove>(); // list of allowed moves not resulting in burn damage
        private List<AllowedMove> _allowedMoves = new List<AllowedMove>(); // list of all allowed moves, including those producing burn damage
        private Stack<AllowedMove> _usedMoves = new Stack<AllowedMove>(); // stack of moves used by the current character
        private List<MapTile> _cursorRange = new List<MapTile>(); // list of MapTiles in the allowed range of the cursor

        public BattleState(InputManager inputManager, StateManager stateManager) : base(inputManager, stateManager)
        {
            _stateType = GameStateType.Battle;
            _moveManager = new BattleMoveManager(_mapReader);
        }
        /// <summary>
        /// Determines whether character has any valid moves remaining this turn.
        /// </summary>
        private bool AnyValidMoves
        {
            get
            {
                return (_allowedMoves.Count > 1) || (_currentCharacter.MoveRemaining >= _minimumActionCost);
            }
        }
        private bool OnOpenTile
        {
            get
            {
                return _currentCharacter.CurrentTile.FriendOnTile == null;
            }
        }
        private bool AttackIsAllowed
        {
            get
            {
                Character target = _cursor.CurrentTile.CharacterOnTile;
                if (target == null) { return false; }
                else
                {
                    return target.Team != _currentCharacter.Team;
                }
            }
        }
        public override void Update()
        {
            if (_mapReader != null)
            {
                _mapReader.UpdateMap();
                switch (_phase)
                {
                    case BattlePhase.Setup:
                        ExecuteSetupPhase();
                        break;
                    case BattlePhase.Action:
                        ExecuteActionPhase();
                        break;
                    case BattlePhase.Cleanup:
                        ExecuteCleanupPhase();
                        break;
                }
            }
            base.Update();
        }
        private void ExecuteSetupPhase()
        {
            AgeFires();
            _mapReader.CharacterList.Sort();
            _currentCharacterIndex = 0;
            _currentCharacter = _mapReader.CharacterList[_currentCharacterIndex];
            _stateManager.UsingViewManager.Tracking1 = _currentCharacter;
            _phase = BattlePhase.Action;
        }
        private void ExecuteActionPhase()
        {
            if (!_moveListsReady)
            {
                if (!_currentCharacter.IsMoving)
                {
                    SetupMoveLists();
                    DarkenForbiddenMoves();
                    HighlightSafeMoves();
                }
            }
            else
            {
                CheckPlayerInput();
            }
        }
        private void SetupMoveLists()
        {
            if (_currentCharacter.CanFly)
            {
                SetupSafeFlyList();
                SetupFullFlyList();
            }
            else
            {
                SetupSafeWalkList();
                SetupFullWalkList();
            }
            _moveListsReady = true;
        }
        /// <summary>
        /// Produces a list of allowed moves for flying units that does not include any burning tiles
        /// </summary>
        private void SetupSafeFlyList()
        {
            float initialRange = _currentCharacter.MoveRemaining;
            float finalRange = initialRange;
            float flyCost = 1.0f;
            MapTile currentTile = _currentCharacter.CurrentTile;
            int listIndex = 0;
            int indexOfNextMoveTile;
            List<MapTile> adjacentTiles;
            AllowedMove nextMove;
            _safeMoves.Clear();
            if (!currentTile.Burning)
            {
                _safeMoves.Add(new AllowedMove(currentTile, initialRange));
            }
            while (listIndex < _safeMoves.Count)
            {
                currentTile = _safeMoves[listIndex].Tile;
                initialRange = _safeMoves[listIndex].RemainingRange;
                adjacentTiles = GetAdjacentTiles(currentTile);
                foreach (MapTile tile in adjacentTiles)
                {
                    finalRange = initialRange - flyCost;
                    if (finalRange >= 0.0f)
                    {
                        if (!tile.Burning && MoveManager.IsFlyOK(_currentCharacter, tile))
                        {
                            nextMove = new AllowedMove(tile, finalRange);
                            indexOfNextMoveTile = IndexOfTileInMoveList(_safeMoves, nextMove);
                            if (indexOfNextMoveTile > -1)
                            {
                                if (IsMoveBetter(_safeMoves, indexOfNextMoveTile, nextMove))
                                {
                                    _safeMoves.Add(nextMove);
                                    _safeMoves.RemoveAt(indexOfNextMoveTile);
                                    if (indexOfNextMoveTile < listIndex)
                                    {
                                        listIndex--;
                                    }
                                }
                            }
                            else
                            {
                                _safeMoves.Add(nextMove);
                            }
                        }
                    }
                }
                listIndex++;
            }
        }
        /// <summary>
        /// Returns a list of MapTiles adjacent to the supplied tile.
        /// </summary>
        /// <param name="currentTile">Tile to check the four sides of</param>
        /// <returns>List of tiles adjacent to currentTile</returns>
        private List<MapTile> GetAdjacentTiles(MapTile currentTile)
        {
            List<MapTile> adjacentTiles = new List<MapTile>();
            MapTile nextTile = _mapReader.GetTileOnRight(currentTile);
            AddIfDifferent(adjacentTiles, nextTile, currentTile);
            nextTile = _mapReader.GetTileOnTop(currentTile);
            AddIfDifferent(adjacentTiles, nextTile, currentTile);
            nextTile = _mapReader.GetTileOnLeft(currentTile);
            AddIfDifferent(adjacentTiles, nextTile, currentTile);
            nextTile = _mapReader.GetTileOnBottom(currentTile);
            AddIfDifferent(adjacentTiles, nextTile, currentTile);
            return adjacentTiles;
        }
        /// <summary>
        /// Adds newTile to tileList if newTile is different from oldTile.
        /// </summary>
        /// <param name="tileList">List of MapTiles to possibly add newTile to</param>
        /// <param name="newTile">Tile to consider adding</param>
        /// <param name="oldTile">Tile to compare newTile to</param>
        /// <returns>The possibly modified list of MapTiles</returns>
        private List<MapTile> AddIfDifferent(List<MapTile> tileList, MapTile newTile, MapTile oldTile)
        {
            if (newTile != oldTile)
            {
                tileList.Add(newTile);
            }
            return tileList;
        }
        /// <summary>
        /// Checks whether the newMove will improve the moveList by
        /// increasing the remaining movement range to a tile already on the list.
        /// </summary>
        /// <param name="moveList">List of current AllowedMoves</param>
        /// <param name="oldMoveIndex">Index in moveList of move to same tile as newMove</param>
        /// <param name="newMove">Move to check against the current list</param>
        /// <returns>Whether the newMove improves the moveList</returns>
        private bool IsMoveBetter(List<AllowedMove> moveList, int oldMoveIndex, AllowedMove newMove)
        {
            AllowedMove oldMove = moveList[oldMoveIndex];
            float oldRemainingRange = oldMove.RemainingRange;
            float newRemainingRange = newMove.RemainingRange;
            return (newRemainingRange > oldRemainingRange);
        }
        /// <summary>
        /// Finds the first move to the same MapTile as newMove in moveList and returns its index in the List.
        /// If the tile is not found, -1 is returned.
        /// </summary>
        /// <param name="moveList">The List to search for a similar move</param>
        /// <param name="newMove">The move containing the tile being searched for</param>
        /// <returns>Index in moveList of move to same tile as newMove; -1 if not found</returns>
        private int IndexOfTileInMoveList(List<AllowedMove> moveList, AllowedMove newMove)
        {
            MapTile newTile = newMove.Tile;
            AllowedMove oldMove;
            int tileInList = -1;
            for (int i = 0; i < moveList.Count; i++)
            {
                oldMove = moveList[i];
                if (oldMove.Tile == newTile)
                {
                    tileInList = i;
                    break;
                }
            }
            return tileInList;
        }
        /// <summary>
        /// Finds the first move to the same MapTile as tileToFind in moveList and returns its index in the List.
        /// If the tile is not found, -1 is returned.
        /// </summary>
        /// <param name="moveList">The List to search for a specific MapTile</param>
        /// <param name="tileToFind">The tile being searched for</param>
        /// <returns>Index in moveList of move to same tile as tileToFind; -1 if not found</returns>
        public int IndexOfTileInMoveList(List<AllowedMove> moveList, MapTile tileToFind)
        {
            AllowedMove oldMove;
            int tileInList = -1;
            for (int i = 0; i < moveList.Count; i++)
            {
                oldMove = moveList[i];
                if (oldMove.Tile == tileToFind)
                {
                    tileInList = i;
                    break;
                }
            }
            return tileInList;
        }
        /// <summary>
        /// Produces a list of allowed moves for walking units that does not include burning tiles
        /// </summary>
        private void SetupSafeWalkList()
        {
            float initialRange = _currentCharacter.MoveRemaining;
            float finalRange = initialRange;
            float walkCost;
            MapTile currentTile = _currentCharacter.CurrentTile;
            int listIndex = 0;
            int indexOfNextMoveTile;
            List<MapTile> adjacentTiles;
            AllowedMove nextMove;
            _safeMoves.Clear();
            if (!currentTile.Burning)
            {
                _safeMoves.Add(new AllowedMove(currentTile, initialRange));
            }
            while (listIndex < _safeMoves.Count)
            {
                currentTile = _safeMoves[listIndex].Tile;
                initialRange = _safeMoves[listIndex].RemainingRange;
                adjacentTiles = GetAdjacentTiles(currentTile);
                foreach (MapTile tile in adjacentTiles)
                {
                    walkCost = tile.WalkCost;
                    finalRange = initialRange - walkCost;
                    if (finalRange >= 0.0f)
                    {
                        if (!tile.Burning && MoveManager.IsWalkOK(_currentCharacter, tile))
                        {
                            nextMove = new AllowedMove(tile, finalRange);
                            indexOfNextMoveTile = IndexOfTileInMoveList(_safeMoves, nextMove);
                            if (indexOfNextMoveTile > -1)
                            {
                                if (IsMoveBetter(_safeMoves, indexOfNextMoveTile, nextMove))
                                {
                                    _safeMoves.Add(nextMove);
                                    _safeMoves.RemoveAt(indexOfNextMoveTile);
                                    if (indexOfNextMoveTile < listIndex)
                                    {
                                        listIndex--;
                                    }
                                }
                            }
                            else
                            {
                                _safeMoves.Add(nextMove);
                            }
                        }
                    }
                }
                listIndex++;
            }
        }
        /// <summary>
        /// Produces a list of allowed moves for flying units, including potentially damaging routes
        /// </summary>
        private void SetupFullFlyList()
        {
            float initialRange = _currentCharacter.MoveRemaining;
            float finalRange = initialRange;
            float flyCost = 1.0f;
            MapTile currentTile = _currentCharacter.CurrentTile;
            int listIndex = 0;
            int indexOfNextMoveTile;
            List<MapTile> adjacentTiles;
            AllowedMove nextMove;
            _allowedMoves.Clear();
            _allowedMoves.Add(new AllowedMove(currentTile, initialRange));
            while (listIndex < _allowedMoves.Count)
            {
                currentTile = _allowedMoves[listIndex].Tile;
                initialRange = _allowedMoves[listIndex].RemainingRange;
                adjacentTiles = GetAdjacentTiles(currentTile);
                foreach (MapTile tile in adjacentTiles)
                {
                    finalRange = initialRange - flyCost;
                    if (finalRange >= 0.0f)
                    {
                        if (MoveManager.IsFlyOK(_currentCharacter, tile))
                        {
                            nextMove = new AllowedMove(tile, finalRange);
                            indexOfNextMoveTile = IndexOfTileInMoveList(_allowedMoves, nextMove);
                            if (indexOfNextMoveTile > -1)
                            {
                                if (IsMoveBetter(_allowedMoves, indexOfNextMoveTile, nextMove))
                                {
                                    _allowedMoves.Add(nextMove);
                                    _allowedMoves.RemoveAt(indexOfNextMoveTile);
                                    if (indexOfNextMoveTile < listIndex)
                                    {
                                        listIndex--;
                                    }
                                }
                            }
                            else
                            {
                                _allowedMoves.Add(nextMove);
                            }
                        }
                    }
                }
                listIndex++;
            }
        }
        /// <summary>
        /// Produces a list of allowed moves for walking units, including potentially damaging routes
        /// </summary>
        private void SetupFullWalkList()
        {
            float initialRange = _currentCharacter.MoveRemaining;
            float finalRange = initialRange;
            float walkCost;
            MapTile currentTile = _currentCharacter.CurrentTile;
            int listIndex = 0;
            int indexOfNextMoveTile;
            List<MapTile> adjacentTiles;
            AllowedMove nextMove;
            _allowedMoves.Clear();
            _allowedMoves.Add(new AllowedMove(currentTile, initialRange));
            while (listIndex < _allowedMoves.Count)
            {
                currentTile = _allowedMoves[listIndex].Tile;
                initialRange = _allowedMoves[listIndex].RemainingRange;
                adjacentTiles = GetAdjacentTiles(currentTile);
                foreach (MapTile tile in adjacentTiles)
                {
                    walkCost = tile.WalkCost;
                    finalRange = initialRange - walkCost;
                    if (finalRange >= 0.0f)
                    {
                        if (MoveManager.IsWalkOK(_currentCharacter, tile))
                        {
                            nextMove = new AllowedMove(tile, finalRange);
                            indexOfNextMoveTile = IndexOfTileInMoveList(_allowedMoves, nextMove);
                            if (indexOfNextMoveTile > -1)
                            {
                                if (IsMoveBetter(_allowedMoves, indexOfNextMoveTile, nextMove))
                                {
                                    _allowedMoves.Add(nextMove);
                                    _allowedMoves.RemoveAt(indexOfNextMoveTile);
                                    if (indexOfNextMoveTile < listIndex)
                                    {
                                        listIndex--;
                                    }
                                }
                            }
                            else
                            {
                                _allowedMoves.Add(nextMove);
                            }
                        }
                    }
                }
                listIndex++;
            }
        }
        /// <summary>
        /// This method causes the safe moves to flash.
        /// </summary>
        private void HighlightSafeMoves()
        {
            foreach (AllowedMove safeMove in _safeMoves)
            {
                safeMove.Tile.Flashing = true;
            }
        }
        /// <summary>
        /// Darkens the moves not in the allowed moves list.
        /// </summary>
        private void DarkenForbiddenMoves()
        {
            foreach(MapTile mapTile in _mapReader.AllTiles)
            {
                if (IndexOfTileInMoveList(_allowedMoves, mapTile) == -1)
                {
                    mapTile.Flashing = false;
                    mapTile.MakeDark();
                }
            }
        }
        private void DarkenAllTiles()
        {
            foreach(MapTile mapTile in _mapReader.AllTiles)
            {
                mapTile.Flashing = false;
                mapTile.MakeDark();
            }
        }
        private void HighlightTargetingRange()
        {
            foreach(MapTile mapTile in _cursorRange)
            {
                mapTile.Flashing = true;
            }
        }
        private void CheckPlayerInput()
        {
            MoveDirection direction;
            Agent agent = _currentCharacter.BattleAgent;
            switch (_actionStep)
            {
                case ActionStep.Moving:
                    direction = GetInputDirectionPressed(agent.AgentName);
                    if (MoveCharacter(_currentCharacter, direction))
                    {
                        _moveListsReady = false;
                    }
                    else if (agent.IsCancelPressed) { ReverseLastMove(); }
                    else if (agent.IsActionPressed || !AnyValidMoves)
                    {
                        if (OnOpenTile)
                        {
                            EndCharacterTurn();
                        }
                    }
                    else if (agent.IsAttackPressed && (_currentCharacter.MoveRemaining >= _attackCost)) { SetupAttackTargeting(); }
                    break;
                case ActionStep.Targeting:
                    agent = _cursor.Agent;
                    direction = GetInputDirectionPressed(agent.AgentName);
                    MoveCursor(direction);
                    if (agent.IsActionPressed || agent.IsAttackPressed)
                    {
                        if (AttackIsAllowed)
                        {
                            ExecuteAttack();
                        }
                    }
                    else if (agent.IsCancelPressed) { EndTargeting(); }
                    break;
            }
        }
        /// <summary>
        /// Ends the current character's turn and transfers control to the next character.
        /// </summary>
        private void EndCharacterTurn()
        {
            _currentCharacter.MoveRemaining = _currentCharacter.MoveRange;
            _usedMoves.Clear();
            _moveListsReady = false;
            if (_currentCharacterIndex < _mapReader.CharacterList.Count - 1)
            {
                _currentCharacterIndex++;
                _currentCharacter = _mapReader.CharacterList[_currentCharacterIndex];
                _stateManager.UsingViewManager.Tracking1 = _currentCharacter;
            }
            else
            {
                _phase = BattlePhase.Cleanup;
            }
        }
        /// <summary>
        /// Moves the current character back to the previously occupied tile.
        /// </summary>
        private void ReverseLastMove()
        {
            if (_usedMoves.Count >= 1)
            {
                AllowedMove lastMove = _usedMoves.Pop();
                _currentCharacter.CurrentTile = lastMove.Tile;
                _currentCharacter.MoveRemaining = lastMove.RemainingRange;
                _moveListsReady = false;
            }
        }
        private void SetupAttackTargeting()
        {
            MapTile currentTile = _currentCharacter.CurrentTile;
            _cursor = new Cursor(_currentCharacter);
            _cursorRange.Add(currentTile);
            _cursorRange.AddRange(GetAdjacentTiles(currentTile));
            DarkenAllTiles();
            HighlightTargetingRange();
            _actionStep = ActionStep.Targeting;
        }
        private void ExecuteAttack()
        {
            Character target = _cursor.CurrentTile.CharacterOnTile;
            int damage = CalculateAttackDamage(_currentCharacter, target);
            Console.WriteLine($"{target.ScreenName} takes {damage} damage!");
            target.HP -= damage;
            if ( target.HP <= 0)
            {
                RemoveCharacter(target);
                Console.WriteLine($"{target.ScreenName} is defeated!");
            }
            _currentCharacter.MoveRemaining -= _attackCost;
            EndTargeting();
        }
        private void EndTargeting()
        {
            _moveListsReady = false;
            _actionStep = ActionStep.Moving;
            _cursor.CurrentTile.CursorOnTile = null;
            _cursor = null;
            _cursorRange.Clear();
        }
        private void RemoveCharacter(Character character)
        {
            List<Character> allCharacters = _mapReader.CharacterList;
            int characterID = character.CharacterID;
            character.CurrentTile.CharacterOnTile = null;
            for (int i = 0; i < allCharacters.Count; i++)
            {
                if (allCharacters[i].CharacterID == characterID)
                {
                    allCharacters.RemoveAt(i);
                    if (i < _currentCharacterIndex) { _currentCharacterIndex--; }
                    else if (i == _currentCharacterIndex)
                    {
                        _currentCharacterIndex--;
                        EndCharacterTurn();
                    }
                }
            }
        }
        /// <summary>
        /// Checks whether character can move in indicated direction and moves it if it can.
        /// </summary>
        /// <param name="character"></param>
        /// <param name="direction"></param>
        /// <returns>Whether character has been instructed to move</returns>
        protected bool MoveCharacter(Character character, MoveDirection direction)
        {
            bool moveSuccess = false;
            AllowedMove lastMove = new AllowedMove(character.CurrentTile, character.MoveRemaining);
            switch (direction)
            {
                case MoveDirection.Right:
                    if (_moveManager.IsMoveRightOK(character, _allowedMoves, this))
                    {
                        _moveManager.MoveRight(character, _allowedMoves, this);
                        moveSuccess = true;
                    }
                    break;
                case MoveDirection.Up:
                    if (_moveManager.IsMoveUpOK(character, _allowedMoves, this))
                    {
                        _moveManager.MoveUp(character, _allowedMoves, this);
                        moveSuccess = true;
                    }
                    break;
                case MoveDirection.Left:
                    if (_moveManager.IsMoveLeftOK(character, _allowedMoves, this))
                    {
                        _moveManager.MoveLeft(character, _allowedMoves, this);
                        moveSuccess = true;
                    }
                    break;
                case MoveDirection.Down:
                    if (_moveManager.IsMoveDownOK(character, _allowedMoves, this))
                    {
                        _moveManager.MoveDown(character, _allowedMoves, this);
                        moveSuccess = true;
                    }
                    break;
            }
            if (moveSuccess)
            {
                _usedMoves.Push(lastMove);
            }
            return moveSuccess;
        }
        private void MoveCursor(MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.Right:
                    if (_moveManager.IsMoveRightOK(_cursor, _cursorRange))
                    {
                        _moveManager.MoveRight(_cursor);
                    }
                    break;
                case MoveDirection.Up:
                    if (_moveManager.IsMoveUpOK(_cursor, _cursorRange))
                    {
                        _moveManager.MoveUp(_cursor);
                    }
                    break;
                case MoveDirection.Left:
                    if (_moveManager.IsMoveLeftOK(_cursor, _cursorRange))
                    {
                        _moveManager.MoveLeft(_cursor);
                    }
                    break;
                case MoveDirection.Down:
                    if (_moveManager.IsMoveDownOK(_cursor, _cursorRange))
                    {
                        _moveManager.MoveDown(_cursor);
                    }
                    break;
            }
        }
        private int CalculateAttackDamage(Character attacker, Character defender)
        {
            float attackModifier = attacker.CurrentTile.AttackDamageModifier;
            float defenseModifier = defender.CurrentTile.DefenseDamageModifier;
            float randomSum = RandomGenerator.NextSingle() + RandomGenerator.NextSingle() + RandomGenerator.NextSingle();
            return (int)Math.Round((Math.Max(attacker.Attack - defender.Defense, 0) + attacker.Level) * attackModifier * defenseModifier * (0.3f + (randomSum * 1.4f / 3.0f)));
        }
        private void ExecuteCleanupPhase()
        {
            _phase = BattlePhase.Setup;
        }
        private void AgeFires()
        {

        }
    }

    public class AllowedMove
    {
        private MapTile _tile;
        private float _remainingRange;
        public AllowedMove(MapTile tile, float remainingRange)
        {
            _tile = tile;
            _remainingRange = remainingRange;
        }

        public MapTile Tile => _tile;
        public float RemainingRange => _remainingRange;
    }

    public enum GameStateType
    {
        Title,
        Explore,
        Configure,
        Battle,
        Pause,
        Save,
        Load
    }

    public enum BattlePhase
    {
        Setup,
        Action,
        Cleanup
    }

    public enum ActionStep
    {
        Moving,
        Targeting,
        ChoosingItem,
        ChoosingMagic
    }
}
