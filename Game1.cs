﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace apt
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        public readonly int _gameWidth = 11 * GameVariables.TileWidth; // width of gameplay area in pixels
        public readonly int _gameHeightMin = Convert.ToInt32(10.25f * GameVariables.TileHeight); // minimum height of gameplay area in pixels
        public readonly int _gameHeightMax = 11 * GameVariables.TileHeight; // maximum height of gameplay area in pixels
        public readonly int _guiWidth = 640;
        public readonly int _guiHeight = 360;
        private TerrainTextureManager _terrainTextureManager;
        private SpriteTextureManager _spriteTextureManager;
        private DisplayManager _displayManager;
        private ViewManager _viewManager;
        private InputManager _inputManager;
        private StateManager _stateManager;
        private FontManager _fontManager;
        private GuiManager _guiManager;
        private MapReader _mapReader;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            _displayManager = new DisplayManager(this, _graphics);
            _inputManager = new InputManager();
            _stateManager = new StateManager(this, _inputManager);
            _fontManager = new FontManager(this);
            _guiManager = new GuiManager(this, _fontManager);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        public bool GameIsPaused { get; set; } = false;

        protected override void Initialize()
        {
            float tfps = 75.0f; // target frames per second
            int ticks = (int)((1.0f / tfps) * 1.0e7f); // ticks are 100 nanoseconds long
            TargetElapsedTime = new TimeSpan(ticks); // adjust frame rate
            _displayManager.InitializeDisplayManager();
            _terrainTextureManager = new TerrainTextureManager();
            _spriteTextureManager = new SpriteTextureManager();
            _viewManager = new ViewManager(_displayManager);
            _stateManager.UsingViewManager = _viewManager;
            _stateManager.UsingGuiManager = _guiManager;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            _terrainTextureManager.LoadTerrainTextures(this);
            _spriteTextureManager.LoadSpriteTextures(this);
            _fontManager.LoadFonts();
            _guiManager.LoadGuiTextures(this);
            _mapReader = new MapReader(_terrainTextureManager, _spriteTextureManager, _inputManager, _viewManager);
            _stateManager.UsingMapReader = _mapReader;
        }

        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            _stateManager.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            SamplerState pixelSampler = SamplerState.PointClamp;

            // Draw Game Area to render target
            Matrix gameScaler = Matrix.CreateScale(_displayManager.GameScale);
            GraphicsDevice.SetRenderTarget(_displayManager.GameRenderTarget);
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin(samplerState: pixelSampler);
            _mapReader.DrawMap(_spriteBatch);
            _spriteBatch.End();

            // Draw GUI to render target
            Matrix guiScaler = Matrix.CreateScale(_displayManager.GuiScale);
            GraphicsDevice.SetRenderTarget(_displayManager.GuiRenderTarget);
            GraphicsDevice.Clear(Color.Transparent);

            _spriteBatch.Begin(samplerState: pixelSampler);
            _guiManager.DrawGui(_spriteBatch);
            _spriteBatch.End();

            // Draw render targets to screen
            GraphicsDevice.SetRenderTarget(null);

            _spriteBatch.Begin(samplerState: pixelSampler, transformMatrix: gameScaler);
            _spriteBatch.Draw(_displayManager.GameRenderTarget,
                _displayManager.GameDestinationRectangle,
                _displayManager.GameSourceRectangle,
                Color.White);
            _spriteBatch.End();

            _spriteBatch.Begin(samplerState: pixelSampler, transformMatrix: guiScaler);
            _spriteBatch.Draw(_displayManager.GuiRenderTarget,
                _displayManager.GuiDestinationRectangle,
                _displayManager.GuiSourceRectangle,
                Color.White);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}