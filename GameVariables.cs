﻿using System;
using System.Collections.Generic;

namespace apt
{
    static internal class GameVariables
    {
        public static int TerrainBGWidth { get; } = 32;
        public static int TerrainBGHeight { get; } = 40;
        public static int TerrainFGWidth { get; } = 32;
        public static int TerrainFGHeight { get; } = 32;
        public static int TileWidth { get; } = 32;
        public static int TileHeight { get; } = 32;
    }
}
