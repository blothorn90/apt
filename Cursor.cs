﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace apt
{
    public class Cursor
    {
        private Texture2D _cursorBGTexture;
        private Texture2D _cursorFGTexture;
        private Agent _agent;
        private Character _character; // character being replaced by cursor
        private static readonly int _width = 32;
        private static readonly int _height = 64;
        private static readonly int _spriteFrames = 2; // number of frames in cursor animation
        private int _spriteStep; // counter for animating cursor sprite
        private int _frameIndex; // index of current frame in cursor animation
        private Rectangle[] _sourceRectangle;
        private Rectangle _destinationRectangle;
        private MapTile _mapTile;
        private int _x; // x-coordinate of left of cursor
        private int _y; // y-coordinate of top of cursor
        public Cursor(Character character)
        {
            _character = character;
            _cursorBGTexture = _character.TextureManager.CursorBGTexture;
            _cursorFGTexture = _character.TextureManager.CursorFGTexture;
            SetupSourceRectangles();
            _mapTile = _character.CurrentTile;
            _mapTile.CursorOnTile = this;
            _x = _mapTile.TileLeft;
            _y = _mapTile.TileBottom - _height;
            _destinationRectangle = new Rectangle(_x, _y, _width, _height);
            _agent = _character.BattleAgent;
        }
        public Agent Agent => _agent;
        public Character Character => _character;
        public MapTile CurrentTile
        {
            get => _mapTile;
            set
            {
                _mapTile.CursorOnTile = null;
                _mapTile = value;
                _mapTile.CursorOnTile = this;
                _x = _mapTile.TileLeft;
                _y = _mapTile.TileBottom - _height;
            }
        }
        private int SpriteSteps => 25; // game steps between sprite frames
        public void DrawCursorBG(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            Rectangle destinationRectangle = new Rectangle();
            destinationRectangle.X = _destinationRectangle.X - viewManager.Left;
            destinationRectangle.Y = _destinationRectangle.Y - viewManager.Top;
            destinationRectangle.Width = _destinationRectangle.Width;
            destinationRectangle.Height = _destinationRectangle.Height;
            spriteBatch.Draw(
                _cursorBGTexture,
                destinationRectangle,
                _sourceRectangle[_frameIndex],
                Color.White
                );
        }
        public void DrawCursorFG(SpriteBatch spriteBatch, ViewManager viewManager)
        {
            Rectangle destinationRectangle = new Rectangle();
            destinationRectangle.X = _destinationRectangle.X - viewManager.Left;
            destinationRectangle.Y = _destinationRectangle.Y - viewManager.Top;
            destinationRectangle.Width = _destinationRectangle.Width;
            destinationRectangle.Height = _destinationRectangle.Height;
            spriteBatch.Draw(
                _cursorFGTexture,
                destinationRectangle,
                _sourceRectangle[_frameIndex],
                Color.White
                );
        }
        private void SetupSourceRectangles()
        {
            int x;
            int width = _width;
            int height = _height;
            _sourceRectangle = new Rectangle[_spriteFrames];
            for (int i = 0; i < _spriteFrames; i++)
            {
                x = i * width;
                _sourceRectangle[i] = new Rectangle(x, 0, width, height);
            }
        }

        public void Update()
        {
            _destinationRectangle.X = _x;
            _destinationRectangle.Y = _y;

            _spriteStep++;
            if (_spriteStep > SpriteSteps)
            {
                _spriteStep = 0;
                _frameIndex++;
                if (_frameIndex >= _spriteFrames)
                {
                    _frameIndex = 0;
                }
            }
        }
    }
}
