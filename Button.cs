﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt
{
    public class Button
    {
        private int _blinkOnTime = 18; // number of game frames active button remains on lit graphics frame (1)
        private int _blinkOffTime = 9; // number of game frames active button remains on unlit graphics frame (0)
        private int _blinkTimer = 0;
        private bool _isActive = false;
        private bool _buttonIsLit = false;
        private Rectangle _sourceRectangleLit;
        private Rectangle _sourceRectangleUnlit;
        private Rectangle _destinationRectangle;
        private Texture2D _buttonTexture;
        public Button(Texture2D buttonTexture) : this(buttonTexture, 0, 0)
        {
        }
        public Button(Texture2D buttonTexture, int x, int y)
        {
            X = x;
            Y = y;
            _buttonTexture = buttonTexture;
            SetupRectangles();
        }
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
                if (_isActive == false)
                {
                    _buttonIsLit = false;
                    _blinkTimer = 0;
                }
            }
        }
        public bool IsLit
        {
            get => _buttonIsLit;
            set => _buttonIsLit = value;
        }
            
        public int X { get; set; }
        public int Y { get; set; }
        public int Width => 17;
        public int Height => 9;
        private void SetupRectangles()
        {
            _sourceRectangleUnlit = new Rectangle(0, 0, Width, Height);
            _sourceRectangleLit = new Rectangle(Width, 0, Width, Height);
            _destinationRectangle = new Rectangle(X, Y, Width, Height);
        }
        public void Update()
        {
            if (IsActive)
            {
                _blinkTimer++;
                if (IsLit)
                {
                    if (_blinkTimer > _blinkOnTime)
                    {
                        _buttonIsLit = false;
                        _blinkTimer = 0;
                    }
                }
                else
                {
                    if (_blinkTimer > _blinkOffTime)
                    {
                        _buttonIsLit = true;
                        _blinkTimer = 0;
                    }
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsLit)
            {
                spriteBatch.Draw(_buttonTexture, _destinationRectangle, _sourceRectangleLit, Color.White);
            }
            else
            {
                spriteBatch.Draw(_buttonTexture, _destinationRectangle, _sourceRectangleUnlit, Color.White);
            }
        }
    }
}
