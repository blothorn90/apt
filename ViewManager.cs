﻿using System;
using System.Collections.Generic;

namespace apt
{
    public class ViewManager
    {
        private DisplayManager _displayManager;
        private int _viewWidth;
        private int _viewHeight;
        private int _x = 0;
        private int _y = 0;
        private Character _tracked1; // the first character for the view manager to track
        private Character _tracked2 = null; // the second character for the view manager to track
        private int _numberTracked = 1;
        private MapReader _mapReader;
        private int _mapWidth;
        private int _mapHeight;
        public ViewManager(DisplayManager displayManager)
        {
            _displayManager = displayManager;
            _viewWidth = _displayManager.GameSourceRectangle.Width;
            _viewHeight = _displayManager.GameSourceRectangle.Height;
            _mapWidth = _viewWidth;
        }

        public int Width
        {
            get
            {
                return _viewWidth;
            }
        }
        public int Height => _viewHeight;
        public int Left => _x;
        public int Top => _y;
        public int Right => Left + Width;
        public int Bottom => Top + Height;
        public Character Tracking1
        {
            get
            {
                return _tracked1;
            }
            set
            {
                _tracked1 = value;
            }
        }
        public Character Tracking2
        {
            get
            {
                return _tracked2;
            }
            set
            {
                _tracked2 = value;
                if (_tracked2 != null)
                {
                    _numberTracked = 2;
                }
                else
                {
                    _numberTracked = 1;
                }
            }
        }

        public void SetMapReader(MapReader mapReader)
        {
            _mapReader = mapReader;
            _mapWidth = _mapReader.MapRightEdge;
            _mapHeight = _mapReader.MapBottomEdge;
        }

        public void Update()
        {
            if (_numberTracked == 1)
            {
                _x = Tracking1?.CenterX - (_viewWidth / 2) ?? 0;
                _y = Tracking1?.CenterY - (_viewHeight / 2) ?? 0;
            }
            else
            {
                int viewCenterX = (Tracking1.CenterX + Tracking2.CenterX) / 2;
                int viewCenterY = (Tracking1.CenterY + Tracking2.CenterY) / 2;
                _x = viewCenterX - (_viewWidth / 2);
                _y = viewCenterY - (_viewHeight / 2);
            }
            if (_x < 0) { _x = 0; }
            if (Right > _mapWidth) { _x = _mapWidth - _viewWidth; }
            if (_y < 0) { _y = 0; }
            if (Bottom > _mapHeight) { _y = _mapHeight - _viewHeight; }
        }
    }
}
