﻿using System;
using System.Collections.Generic;

namespace apt
{
    public abstract class MoveManager
    {
        protected MapReader _mapReader;
        public MoveManager(MapReader mapReader)
        {
            _mapReader = mapReader;
        }

        public MapReader UsingMapReader
        {
            get
            {
                return _mapReader;
            }
            set
            {
                _mapReader = value;
            }
        }
        public abstract bool IsMoveRightOK(Character character);
        public abstract bool IsMoveUpOK(Character character);
        public abstract bool IsMoveLeftOK(Character character);
        public abstract bool IsMoveDownOK(Character character);
        public virtual bool IsMoveRightOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            return IsMoveRightOK(character);
        }
        public virtual bool IsMoveUpOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            return IsMoveUpOK(character);
        }
        public virtual bool IsMoveLeftOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            return IsMoveLeftOK(character);
        }
        public virtual bool IsMoveDownOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            return IsMoveDownOK(character);
        }
        public virtual bool IsMoveRightOK(Cursor cursor, List<MapTile> cursorRange) { return true; }
        public virtual bool IsMoveUpOK(Cursor cursor, List<MapTile> cursorRange) { return true; }
        public virtual bool IsMoveLeftOK(Cursor cursor, List<MapTile> cursorRange) { return true; }
        public virtual bool IsMoveDownOK(Cursor cursor, List<MapTile> cursorRange) { return true; }
        public static bool IsTileBlocked(Character character, MapTile tile)
        {
            TeamName teamMoving = character.Team;
            TeamName teamStanding = tile.CharacterOnTile?.Team ?? TeamName.None;
            if ((teamStanding == TeamName.None) || (teamMoving == teamStanding))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool IsWalkOK(Character character, MapTile tile)
        {
            if ((tile.Walkable) && (!IsTileBlocked(character, tile)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool IsFlyOK(Character character, MapTile tile)
        {
            if (!IsTileBlocked(character, tile))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsMoveOK(Character character, MapTile tile)
        {
            if (character.CanFly)
            {
                return IsFlyOK(character, tile);
            }
            else
            {
                return IsWalkOK(character, tile);
            }
        }
        public void MoveRight(Character character)
        {
            character.FacingDirection = MoveDirection.Right;
            character.CurrentTile = _mapReader.GetTileOnRight(character.CurrentTile);
        }
        public void MoveRight(Cursor cursor) { cursor.CurrentTile = _mapReader.GetTileOnRight(cursor.CurrentTile); }
        public void MoveUp(Character character)
        {
            character.SwitchTileSlow = true;
            character.FacingDirection = MoveDirection.Up;
            character.CurrentTile = _mapReader.GetTileOnTop(character.CurrentTile);
        }
        public void MoveUp(Cursor cursor) { cursor.CurrentTile = _mapReader.GetTileOnTop(cursor.CurrentTile); }
        public void MoveLeft(Character character)
        {
            character.FacingDirection = MoveDirection.Left;
            character.CurrentTile = _mapReader.GetTileOnLeft(character.CurrentTile);
        }
        public void MoveLeft(Cursor cursor) { cursor.CurrentTile = _mapReader.GetTileOnLeft(cursor.CurrentTile); }
        public void MoveDown(Character character)
        {
            character.SwitchTileFast = true;
            character.FacingDirection = MoveDirection.Down;
            character.CurrentTile = _mapReader.GetTileOnBottom(character.CurrentTile);
        }
        public void MoveDown(Cursor cursor) { cursor.CurrentTile = _mapReader.GetTileOnBottom(cursor.CurrentTile); }
        public virtual void MoveRight(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MoveRight(character);
        }
        public virtual void MoveUp(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MoveUp(character);
        }
        public virtual void MoveLeft(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MoveLeft(character);
        }
        public virtual void MoveDown(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MoveDown(character);
        }
    }

    public class ExploreMoveManager : MoveManager
    {
        public ExploreMoveManager(MapReader mapReader) : base(mapReader)
        {

        }
        public override bool IsMoveRightOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnRight(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
        public override bool IsMoveUpOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnTop(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
        public override bool IsMoveLeftOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnLeft(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
        public override bool IsMoveDownOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnBottom(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
    }
    public class BattleMoveManager : MoveManager
    {
        public BattleMoveManager(MapReader mapReader) : base(mapReader)
        {

        }
        public override bool IsMoveRightOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnRight(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
        public override bool IsMoveRightOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnRight(character.CurrentTile);
            if (newTile == character.CurrentTile)
            {
                return false;
            }
            else
            {
                if (battleState.IndexOfTileInMoveList(allowedMoves, newTile) == -1)
                {
                    return false;
                }
                else
                {
                    return IsMoveRightOK(character);
                }
            }
        }
        public override bool IsMoveRightOK(Cursor cursor, List<MapTile> cursorRange)
        {
            MapTile newTile = _mapReader.GetTileOnRight(cursor.CurrentTile);
            if (newTile == cursor.CurrentTile)
            {
                return false;
            }
            else
            {
                if (TileIsInRange(newTile, cursorRange)) { return true; }
                else { return false; }
            }
        }
        public override bool IsMoveUpOK(Cursor cursor, List<MapTile> cursorRange)
        {
            MapTile newTile = _mapReader.GetTileOnTop(cursor.CurrentTile);
            if (newTile == cursor.CurrentTile)
            {
                return false;
            }
            else
            {
                if (TileIsInRange(newTile, cursorRange)) { return true; }
                else { return false; }
            }
        }
        public override bool IsMoveLeftOK(Cursor cursor, List<MapTile> cursorRange)
        {
            MapTile newTile = _mapReader.GetTileOnLeft(cursor.CurrentTile);
            if (newTile == cursor.CurrentTile)
            {
                return false;
            }
            else
            {
                if (TileIsInRange(newTile, cursorRange)) { return true; }
                else { return false; }
            }
        }
        public override bool IsMoveDownOK(Cursor cursor, List<MapTile> cursorRange)
        {
            MapTile newTile = _mapReader.GetTileOnBottom(cursor.CurrentTile);
            if (newTile == cursor.CurrentTile)
            {
                return false;
            }
            else
            {
                if (TileIsInRange(newTile, cursorRange)) { return true; }
                else { return false; }
            }
        }
        /// <summary>
        /// Checks whether tileToFind is in the supplied range.
        /// </summary>
        /// <param name="mapTile"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        private bool TileIsInRange(MapTile tileToFind, List<MapTile> range)
        {
            foreach (MapTile mapTile in range)
            {
                if (mapTile == tileToFind) { return true; }
            }
            return false;
        }
        public override bool IsMoveUpOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnTop(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
        public override bool IsMoveUpOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnTop(character.CurrentTile);
            if (newTile == character.CurrentTile)
            {
                return false;
            }
            else
            {
                if (battleState.IndexOfTileInMoveList(allowedMoves, newTile) == -1)
                {
                    return false;
                }
                else
                {
                    return IsMoveUpOK(character);
                }
            }
        }
        public override bool IsMoveLeftOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnLeft(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
        public override bool IsMoveLeftOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnLeft(character.CurrentTile);
            if (newTile == character.CurrentTile)
            {
                return false;
            }
            else
            {
                if (battleState.IndexOfTileInMoveList(allowedMoves, newTile) == -1)
                {
                    return false;
                }
                else
                {
                    return IsMoveLeftOK(character);
                }
            }
        }
        public override bool IsMoveDownOK(Character character)
        {
            if (!character.IsMoving)
            {
                MapTile newTile = _mapReader.GetTileOnBottom(character.CurrentTile);
                return IsMoveOK(character, newTile);
            }
            else
            {
                return false;
            }
        }
        public override bool IsMoveDownOK(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnBottom(character.CurrentTile);
            if (newTile == character.CurrentTile)
            {
                return false;
            }
            else
            {
                if (battleState.IndexOfTileInMoveList(allowedMoves, newTile) == -1)
                {
                    return false;
                }
                else
                {
                    return IsMoveDownOK(character);
                }
            }
        }
        public override void MoveRight(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnRight(character.CurrentTile);
            AllowedMove currentMove = allowedMoves[battleState.IndexOfTileInMoveList(allowedMoves, newTile)];
            character.MoveRemaining = currentMove.RemainingRange;
            MoveRight(character);
        }
        public override void MoveUp(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnTop(character.CurrentTile);
            AllowedMove currentMove = allowedMoves[battleState.IndexOfTileInMoveList(allowedMoves, newTile)];
            character.MoveRemaining = currentMove.RemainingRange;
            MoveUp(character);
        }
        public override void MoveLeft(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnLeft(character.CurrentTile);
            AllowedMove currentMove = allowedMoves[battleState.IndexOfTileInMoveList(allowedMoves, newTile)];
            character.MoveRemaining = currentMove.RemainingRange;
            MoveLeft(character);
        }
        public override void MoveDown(Character character, List<AllowedMove> allowedMoves, BattleState battleState)
        {
            MapTile newTile = _mapReader.GetTileOnBottom(character.CurrentTile);
            AllowedMove currentMove = allowedMoves[battleState.IndexOfTileInMoveList(allowedMoves, newTile)];
            character.MoveRemaining = currentMove.RemainingRange;
            MoveDown(character);
        }
    }

    public enum MoveDirection
    {
        None,
        Right,
        Up,
        Left,
        Down
    }
}
