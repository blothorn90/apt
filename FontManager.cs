﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace apt
{
    public class FontManager
    {
        private SpriteFont _mainFont;
        private Game1 _game1;
        public FontManager(Game1 game1)
        {
            _game1 = game1;
        }

        public SpriteFont CurrentFont
        {
            get { return _mainFont; }
        }

        public void LoadFonts()
        {
            _mainFont = _game1.Content.Load<SpriteFont>("aptTextFont01");
        }
    }
}
